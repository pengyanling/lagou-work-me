package com.lagou.edu;

import java.util.Arrays;

public class WorkTwo {

    public static void main(String[] args) {
        int w = 10;
        int[] weight = {500, 400, 350, 300, 200};//总共5个金矿，且每个金矿黄金储量不同
        int[] person = {5, 5, 4, 3, 3};//开采对应金矿所需人的数量
        System.out.println("最优收益：" + getBest(w, person, weight));
    }

    /**
     * @param n      最大人数
     * @param person 开采对应金矿所需工人的数量
     * @param weight 金矿
     * @return
     */
    public static int getBest(int n, int[] person, int[] weight) {
        //创建当前结果
        int[] results = new int[n + 1];
        //i从1开始
        for (int i = 1; i <= weight.length; i++) {
            for (int j = n; j >= 1; j--) {
                if (j >= person[i - 1]) {//工人数量 >= 开采该金矿所需工人数量时，才可以开采该金矿：weight[i - 1]

                    //j - person[i - 1]：j个人开采金矿i-1时，空闲人数
                    results[j] = Math.max(results[j], results[j - person[i - 1]] + weight[i - 1]);
                }
            }
        }
        System.out.println("results的结果：");
        for (int result : results) {
            System.out.print(result +" ");
        }
        System.out.println();
        //返回最后一个格子的值
        return results[n];
    }
}
