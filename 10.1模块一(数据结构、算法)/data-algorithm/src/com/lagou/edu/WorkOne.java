package com.lagou.edu;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author pengyanling
 * @createTime 2021年01月01日 13:27.
 */
public class WorkOne {
    public static void main(String[] args) {
        System.out.println(check(new int[]{1, 2, 3}));
        System.out.println(check(new int[]{1, 2, 1}));
        System.out.println(check(new int[]{100, 100}));

    }

    public static String check(int arr[]) {
        HashSet<String> set = new HashSet<>(arr.length);
        for (int num : arr) {
            if (set.contains(num + "")) {
                return "YES";
            }
            set.add(num + "");
        }
        return "NO";
    }
}
