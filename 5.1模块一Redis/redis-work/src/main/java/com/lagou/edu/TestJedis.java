package com.lagou.edu;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * @author pengyanling
 * @createTime 2020年10月20日 15:52.
 */
public class TestJedis {
    public static void main(String[] args) {
        JedisPoolConfig config = new JedisPoolConfig();

        Set<HostAndPort> jedisClusterNode = new HashSet<HostAndPort>();
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6318));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6319));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6328));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6329));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6338));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6339));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6348));
        jedisClusterNode.add(new HostAndPort("192.168.0.128", 6349));
        JedisCluster jcd = new JedisCluster(jedisClusterNode, 2000, 2000, 5, "redis_test", config);
        jcd.set("name:001", "zhangfei121212");
        String value = jcd.get("name:001");
        System.out.println(value);
    }
}
