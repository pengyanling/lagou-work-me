参考地址：https://blog.csdn.net/pyl574069214/article/details/106794897

```properties
启动docker: systemctl start docker
```

```yml
version: "3.4"
services:
  redis:
    image: daocloud.io/library/redis:5.0.7
    restart: always
    container_name: redis
    environment:
      - TZ=Asiz/Shanghai
    ports:
      - 6379:6379
```

启动容器: docker-compose up -d