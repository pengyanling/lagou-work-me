/*
创建数据库test
*/
create database `test` character set utf8 collate utf8_general_ci;
SET FOREIGN_KEY_CHECKS=0;
/*
使用数据库test
*/
use test;

-- ----------------------------
-- Table structure for tb_resume
-- ----------------------------
DROP TABLE IF EXISTS `tb_resume`;
CREATE TABLE `tb_resume` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_resume
-- ----------------------------
INSERT INTO `tb_resume` VALUES ('2', '上海', '李四1', '151000000');
INSERT INTO `tb_resume` VALUES ('3', '广州', '王五', '153000000');
INSERT INTO `tb_resume` VALUES ('11', '成都全文', '李留为', '48511343');
INSERT INTO `tb_resume` VALUES ('15', 'asd', 'af', '356');
