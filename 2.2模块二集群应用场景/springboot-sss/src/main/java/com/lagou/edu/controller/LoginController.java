package com.lagou.edu.controller;

import com.lagou.edu.dao.ResumeDao;
import com.lagou.edu.pojo.Result;
import com.lagou.edu.pojo.Resume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {


    @Autowired
    private ResumeDao resumeDao;


    @RequestMapping("/toLogin")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        return "/WEB-INF/success.jsp";
    }
    @RequestMapping("/resumes")
    @ResponseBody
    public List<Resume> resumes(HttpServletRequest request, HttpServletResponse response) {
        return resumeDao.findAll();
    }
    @PostMapping("/add")
    public @ResponseBody
    Result insert(@RequestBody Resume resume) {
        resumeDao.save(resume);
        return new Result("add success");
    }
    @PostMapping("/mod")
    public @ResponseBody
    Result mod(@RequestBody Resume resume) {
        resumeDao.save(resume);
        return new Result("modify success");
    }
    @GetMapping("/del/{id}")
    public @ResponseBody
    Result delete(@PathVariable("id") Long id) {
        resumeDao.deleteById(id);
        return new Result("delete success") ;
    }
}
