<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>查看简历列表</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <style type="text/css">
        table{
            border: 1px solid #d9cece;
        }
        th,td{
            width: 100px;
            border: 1px solid #d9cece;
            text-align: center;
        }
        .saveBtn{
            display: none;
        }
    </style>
</head>
<body>
<h2>我是服务器：${pageContext.request.localPort}</h2>
<h2>当前sessionId：${pageContext.session.id}</h2>
登录成功！
<div style="height: 95%">
<div><a href="#" id="addBtn">新增</a></div>
    <table id="resumeTable"></table>
</div>
<script>
    $(function () {
        initTable();

        $(".modBtn").on("click",function(){
            $(this).parents("tr").find("input[name !='id']").removeAttr("readonly");
            $(this).next(".saveBtn").show();
            $(this).hide();
        })
        $(".saveBtn").on("click",function(){
            var allInput = $(this).parents("tr").find("input");
            saveResume(allInput);
        });
        $(".delBtn").on("click",function(){
            var idInput = $(this).parents("tr").find("input[name ='id']");
            var id =$(idInput).val();
            delResume(id);
        });
        $("#addBtn").on("click",function(){
            var aa = "<tr> "+
                "<td><input name='id'  /></td>"+
                "<td><input name='name'   /></td>"+
                "<td><input name='address'  /></td>"+
                "<td><input name='phone'  /></td>"+
                "<td><a href='javascript:;' onclick='addSaveResume(this)' >保存</a></td>"+
                "</tr>";
            $("#resumeTable").append(aa);
        })
    });

    function addSaveResume(_this) {
        var allInput = $(_this).parents("tr").find("input");
        var  id =$(allInput[0]).val();
        var  name =$(allInput[1]).val();
        var  address =$(allInput[2]).val();
        var  phone =$(allInput[3]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/login/add',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"id":id,"name":name,
                "address":address,"phone":phone}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function delResume(id) {
        $.ajax({
            url: '${pageContext.request.contextPath}/login/del/'+id,
            type: 'get',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function saveResume(allInput) {
        var  id =$(allInput[0]).val();
        var  name =$(allInput[1]).val();
        var  address =$(allInput[2]).val();
        var  phone =$(allInput[3]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/login/mod',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"id":id,"name":name,
                "address":address,"phone":phone}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function initTable() {
        $.ajax({
            url: '${pageContext.request.contextPath}/login/resumes',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                $("#resumeTable").empty();
                var aa="<tr><th>Id</th><th>姓名</th><th>地址</th><th>电话</th><th>操作</th></tr>";
                for (var i=0;i<data.length;i++) {
                    aa += "<tr> "+
                            "<td><input name='id' readonly value='"+data[i].id+"' /></td>"+
                            "<td><input name='name' readonly value='"+data[i].name+"' /></td>"+
                            "<td><input name='address' readonly value='"+data[i].address+"' /></td>"+
                            "<td><input name='phone' readonly value='"+data[i].phone+"' /></td>"+
                            "<td><a href='#' class='modBtn'>编辑</a>" +
                                 "<a href='#' class='saveBtn'>保存</a>&nbsp;&nbsp;&nbsp;&nbsp;" +
                                 "<a href='#' class='delBtn'>删除</a></td>"+
                          "</tr>";
                }
                $("#resumeTable").append(aa);
            }
        })
    }
</script>
</body>
</html>