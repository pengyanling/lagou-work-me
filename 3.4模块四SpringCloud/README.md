##### 1、要在hosts添加配置

hosts在C:\Windows\System32\drivers\etc目录下

在hosts文件中添加如下配置：

```sh
127.0.0.1 LagouEurekaServerA 
127.0.0.1 LagouEurekaServerB 
#不然，eureka client找不到eureka server
```

##### 2、配置中心修改

当git的配置修改时后：用Postman发送post请求更新配置：http://localhost:9006/actuator/bus-refresh，好让每一个客户端配置都可自动刷新