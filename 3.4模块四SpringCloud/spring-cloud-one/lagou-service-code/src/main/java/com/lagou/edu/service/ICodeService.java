package com.lagou.edu.service;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:16.
 */
public interface ICodeService {
    boolean createCode(String email);

    int validateCode(String email, String code);
}
