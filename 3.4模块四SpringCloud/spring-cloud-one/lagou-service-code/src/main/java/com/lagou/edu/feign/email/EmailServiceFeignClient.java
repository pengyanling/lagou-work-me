package com.lagou.edu.feign.email;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 21:56.
 */
// 原来：http://lagou-service-email/email/{email}/{code};
// @FeignClient表明当前类是一个Feign客户端，value指定该客户端要请求的服务名称（登记到注册中心上的服务提供者的服务名称）
@FeignClient(value = "lagou-service-email", fallback = EmailFallback.class, path = "/email")
public interface EmailServiceFeignClient {
    // Feign要做的事情就是，拼装url发起请求
    // 我们调用该方法就是调用本地接口方法，那么实际上做的是远程请求
    @GetMapping("/{email}/{code}")
    public boolean sendSimpleMail(@PathVariable("email") String email,
                                          @PathVariable("code") String code);
}
