package com.lagou.edu.feign.email;

import org.springframework.stereotype.Component;

/**
 * 降级回退逻辑
 * @author pengyanling
 * @createTime 2020年08月27日 21:56.
 */
@Component
public class EmailFallback implements EmailServiceFeignClient{
    @Override
    public boolean sendSimpleMail(String email, String code) {
        return false;
    }
}
