package com.lagou.edu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

/**
 *
 * @author pengyanling
 * @createTime 2020年08月28日 13:55.
 */
//@Configuration 暂时没有这个类
//@RefreshScope
public class DataSourceConfig {
    @Bean
    @ConfigurationProperties(prefix = "mysql")
    public DataSource dataSource(){
        return DataSourceBuilder.create().build() ;
    }
}
