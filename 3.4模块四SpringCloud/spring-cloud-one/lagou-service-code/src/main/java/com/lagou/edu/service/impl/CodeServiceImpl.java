package com.lagou.edu.service.impl;

import com.lagou.edu.feign.email.EmailServiceFeignClient;
import com.lagou.edu.mapper.CodeMapper;
import com.lagou.edu.pojo.AuthCode;
import com.lagou.edu.service.ICodeService;
import com.lagou.edu.util.CodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Random;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:18.
 */
@Service
public class CodeServiceImpl implements ICodeService {
    @Autowired
    private CodeMapper codeMapper;
    @Autowired
    private EmailServiceFeignClient emailServiceFeignClient;

    @Override
    @Transactional
    public boolean createCode(String email) {
        AuthCode dbCode = codeMapper.findCode(email);
        String newCode = null;
        if (dbCode != null) {
            if (dbCode.getExpiretime().compareTo(new Date()) <= 0) {
                //验证码已过期，重新生成
                newCode = newCode(email, true);
                emailServiceFeignClient.sendSimpleMail(email, newCode);
            } else {
                //未过期，直接发送邮件
                newCode = dbCode.getCode();
                emailServiceFeignClient.sendSimpleMail(email, newCode);
            }
        } else {
            //没有生成过验证码，需要生成验证码
            newCode = newCode(email, false);
            emailServiceFeignClient.sendSimpleMail(email, newCode);
        }
        return newCode != null ? true : false;
    }

    @Override
    public int validateCode(String email, String code) {
        return CodeUtils.validateCode(codeMapper.findCode(email), code);
    }

    /**
     * @param email
     * @param isUpdate 是否是更新验证码
     * @return
     */
    private String newCode(String email, boolean isUpdate) {
        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            code.append(random.nextInt(10));
        }
        AuthCode codeBean = new AuthCode();
        codeBean.setCode(code.toString());
        codeBean.setCreatetime(new Date());
        //10分钟过期
        codeBean.setExpiretime(new Date(codeBean.getCreatetime().getTime() + 1000 * 60 * 10));
        codeBean.setEmail(email);
        if (isUpdate) {
            codeMapper.modCode(codeBean);
        } else {
            codeMapper.addCode(codeBean);
        }
        return codeBean.getCode();
    }
}
