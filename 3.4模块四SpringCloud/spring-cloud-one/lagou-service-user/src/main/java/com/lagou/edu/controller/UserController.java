package com.lagou.edu.controller;

import com.lagou.edu.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:08.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    @PostMapping("/register/{email}/{password}/{code}")
    public boolean register(@PathVariable String email,
                            @PathVariable String password,
                            @PathVariable String code) {
        return userService.register(email, password, code, response);
    }

    @GetMapping("/isRegistered/{email}")
    public boolean isRegistered(@PathVariable String email) {
        return userService.isRegistered(email);
    }

    @PostMapping("/login/{email}/{password}")
    public String login(@PathVariable String email,
                        @PathVariable String password) {
        return userService.login(email, password, response);
    }

    @GetMapping("/info/{token}")
    public String info(@PathVariable String token) {
        return userService.info(token);
    }
}
