package com.lagou.edu.pojo;

import lombok.Data;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:34.
 */
@Data
public class LagouToken {
    /**
     * 自增主键
     */
    private int id;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 令牌
     */
    private String token;
}
