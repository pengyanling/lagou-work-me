package com.lagou.edu.mapper;

import com.lagou.edu.pojo.LagouToken;
import org.apache.ibatis.annotations.Param;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:20.
 */
public interface TokenMapper {
    LagouToken findByEmail(@Param("email") String email);
    String findByToken(@Param("token") String token);

    int addToken(LagouToken tokenBean);

    int modToken(LagouToken tokenBean);
}
