package com.lagou.edu.exception;


import com.lagou.edu.param.ErrorCode;
import com.lagou.edu.param.ErrorCodeEnum;

/**
 * 业务异常类
 */
public class ApiException extends GlobalException {

    private static final long serialVersionUID = 1L;


    /**
     * 错误码
     */
    private final ErrorCode errorCode;

    public ApiException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.msg());
        this.errorCode = errorCodeEnum.convert();
    }

    public ApiException(ErrorCode errorCode) {
        super(errorCode.getError());
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

}
