package com.lagou.edu.exception;

/**
 * 业务异常
 */
public class BusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8032514884575906781L;

	public BusinessException(String message){
		super(message);
	}
}
