package com.lagou.edu.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:34.
 */
@Data
public class AuthCode {
    /**
     * 自增主键
     */
    private int id;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 验证码
     */
    private String code;


    /**
     *创建时间
     */
    private Date createtime;


    /**
     * 过期时间
     */
    private Date expiretime;
}
