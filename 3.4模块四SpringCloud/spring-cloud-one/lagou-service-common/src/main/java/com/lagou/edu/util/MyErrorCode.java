package com.lagou.edu.util;


import com.lagou.edu.param.ErrorCode;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by aa on 2019/5/27.
 */
public class MyErrorCode {
    /**
     * 获取ErrorCode-自定义errorcode
     *
     * @param errorCode
     * @param show      是否显示
     * @param error     异常类名称
     * @param meg       错误消息
     * @return
     */
    public static ErrorCode getErrorCode(int errorCode, boolean show, String error, String meg) {
        return ErrorCode.builder().httpCode(errorCode).show(show).error(error).msg(meg).build();
    }

    /**
     * 获取ErrorCode-404
     *
     * @param show  是否显示
     * @param error 异常类名称
     * @param meg   错误消息
     * @return
     */
    public static ErrorCode getErrorCode404(boolean show, String error, String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_NOT_FOUND).show(show).error(error).msg(meg).build();
    }

    /**
     * 获取ErrorCode-404
     *
     * @param show 是否显示
     * @param meg  错误消息
     * @return
     */
    public static ErrorCode getErrorCode404(boolean show, String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_NOT_FOUND).show(show).error("MyBusinessException").msg(meg).build();
    }

    /**
     * 获取ErrorCode-400【语法有误】
     *
     * @param show  是否显示
     * @param error 异常类名称
     * @param meg   错误消息
     * @return
     */
    public static ErrorCode getErrorCode400(boolean show, String error, String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_BAD_REQUEST).show(show).error(error).msg(meg).build();
    }

    /**
     * 获取ErrorCode-400【语法有误】
     *
     * @param show 是否显示
     * @param meg  错误消息
     * @return
     */
    public static ErrorCode getErrorCode400(boolean show, String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_BAD_REQUEST).show(show).error(meg).build();
    }

    /**
     * 默认显示，获取ErrorCode-400【语法有误】
     *
     * @param meg 错误消息
     * @return
     */
    public static ErrorCode getErrorCode400(String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_BAD_REQUEST).show(true).error("MyBusinessException").msg(meg).build();
    }

    /**
     * 获取ErrorCode默认
     *
     * @param meg 错误消息
     * @return
     */
    public static ErrorCode getErrorCodeDef(String meg) {
        return ErrorCode.builder().httpCode(HttpServletResponse.SC_NOT_FOUND).show(true).error("MyBusinessException").msg(meg).build();
    }
}
