package com.lagou.edu.controller;

import com.lagou.edu.email.IMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 21:33.
 */
@RestController
@RequestMapping("/email")
public class EmailController {
    @Autowired
    private IMailService mailService;

    @GetMapping("/{email}/{code}")
    public boolean sendSimpleMail(@PathVariable String email, @PathVariable String code) {
        return mailService.sendSimpleMail(email, "作业：验证码",
                "【Spring Cloud One作业】验证码是：" + code + "，用于注册/登录，10分钟内有效。");
    }
}
