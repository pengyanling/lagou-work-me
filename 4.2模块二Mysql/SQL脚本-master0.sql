/*
创建数据库master0
*/
create database `master0` character set utf8 collate utf8_general_ci;
SET FOREIGN_KEY_CHECKS=0;
/*
使用数据库master0
*/
use master0;
-- ----------------------------
-- 先删表，再建表
-- ----------------------------
DROP TABLE IF EXISTS `c_order`;
CREATE TABLE `c_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`is_del` bit(1) NOT NULL DEFAULT 0 COMMENT '是否被删除',
`user_id` int(11) NOT NULL COMMENT '用户id',
`company_id` int(11) NOT NULL COMMENT '公司id',
`publish_user_id` int(11) NOT NULL COMMENT 'B端用户id',
`position_id` int(11) NOT NULL COMMENT '职位ID',
`resume_type` int(2) NOT NULL DEFAULT 0 COMMENT '简历类型：0附件 1在线',
`status` varchar(256) NOT NULL COMMENT '投递状态 WAIT-待处理 AUTO_FILTER-自动过滤 PREPARE_CONTACT-待沟通 REFUSE-拒绝 ARRANGE_INTERVIEW-通知面试',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_time` datetime NOT NULL COMMENT '处理时间',
PRIMARY KEY (`id`),
KEY `index_userId_positionId` (`user_id`, `position_id`),
KEY `idx_userId_operateTime` (`user_id`, `update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;