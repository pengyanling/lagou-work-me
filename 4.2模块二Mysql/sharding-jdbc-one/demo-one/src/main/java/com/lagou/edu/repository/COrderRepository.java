package com.lagou.edu.repository;

import com.lagou.edu.entity.COrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface COrderRepository extends JpaRepository<COrder,Long> {
}
