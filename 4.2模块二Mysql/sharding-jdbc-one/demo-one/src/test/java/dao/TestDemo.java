package dao;

import com.lagou.edu.RunBoot;
import com.lagou.edu.entity.COrder;
import com.lagou.edu.repository.COrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestDemo {

    @Resource
    private COrderRepository cOrderRepository;

    @Test
    public void testAdd() {
        for (int i = 1; i <= 20; i++) {
            COrder order = new COrder();
            order.setUserId(i);
            order.setStatus("WAIT");
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            cOrderRepository.save(order);
        }
    }

    @Test
    public void testLoad() {
        List<COrder> list = cOrderRepository.findAll();
        System.err.println("=======================================================");
        list.forEach(one -> {
            System.out.println(one);
        });
    }
    @Test
    public void testDelAll(){
        cOrderRepository.deleteAll();
    }

}
