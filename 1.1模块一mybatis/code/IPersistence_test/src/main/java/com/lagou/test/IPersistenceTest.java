package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class IPersistenceTest {
    private IUserDao userDao;
    @Before
    public void before()throws Exception {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        userDao = sqlSession.getMapper(IUserDao.class);

    }

    @Test
    public void testInsert()  {
        //调用
        User user = new User();
        user.setId(56);
        user.setUsername("wang56");
        System.out.println(userDao.insert(user));

    }
    @Test
    public void testUpdate()  {
        //调用
        User user = new User();
        user.setId(56);
        user.setUsername("lili56");
        System.out.println(userDao.update(user));

    }
    @Test
    public void testDelete()  {
        //调用
        User user = new User();
        user.setId(56);
        user.setUsername("lili56");
        System.out.println(userDao.delete(user));

    }
    @Test
    public void testSelectOne() throws Exception {
        //调用
        User user = new User();
        user.setId(4);
        user.setUsername("zhangsan");
        User userZs = userDao.findByCondition(user);
        System.out.println(userZs);

    }
}
