package com.lagou.xml;

/**
 * @author pengyanling
 * @createTime 2020年06月24日 16:19.
 */
public class BuilderException extends RuntimeException {
    public BuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
