package com.lagou.pojo;

/**
 * @author pengyanling
 * @createTime 2020年06月24日 15:50.
 */
public enum SqlCommandType {
    UNKNOWN, INSERT, UPDATE, DELETE, SELECT, FLUSH;
}
