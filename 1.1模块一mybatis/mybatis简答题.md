### 一、简答题

######     1、Mybatis动态sql是做什么的？都有哪些动态sql？简述一下动态sql的执行原理？

------

```yml
##Mybatis动态sql：
是根据传入的参数对象或参数值，根据匹配条件，拼接对应的sql语句。
##动态sql标签有：
if、where、include,choose、when、otherwise,foreach,set,trim
##执行原理：
        1、在解析配置文件时，构建XMLMapperBuilder，解析所有的mapper配置，并与解析内容存入Configuration的mappedStatements中；
        2、在XMLMapperBuilder的configurationElement方法中对select|insert|update|delete标签进行了解析；将标签内容存入XNode的body属性
        3、在XNode的构造器中调用了parseBody方法，调用PropertyParser.parse方法，构造GenericTokenParser对象，对象中含：开始标记、结束标记、变量对象(TokenHandler)
        4、执行GenericTokenParser的parse方法，在这个方法中调用了SqlSourceBuilder的内部类ParameterMappingTokenHandler的handleToken方法
        5、在handleToken方法中将参数类型、参数名称、封装到ParameterMapping类中，并将sql中的占位符替换成?。
        6、执行过程中,调用MappedStatement的getBoundSql方法，
调用DynamicSqlSource的getBoundSql方法获取BoundSql，在getBoundSql方法中对参数、参数值进行绑定
```

解析配置文件时：

mybatis动态sql的源码分析流程

![](../../images/mybatis动态sql的源码分析流程.png)

执行(select、insert、update、delete)方法时的源码流程：

![](../../images/执行(select、insert、update、delete)方法时的源码流程.png)

######         2、Mybatis是否支持延迟加载？如果支持，它的实现原理是什么？

```yml
mybatis仅支持association关联对象、collection关联计划对象的延迟加载
association：指的是一对一查询
collection：指的是一对多查询
在mybatis配置文件中，可以配置是否启用延迟加载lazyLoadingEnabled=true或者false
##实现原理
使用CGLIB的代理方式代理目标对象，当调用目标方法时，进入代理对象方法：intercept方法(相当于Jdk代理的invoke方法)，比如调用a.getB().getName(),拦截器intercept()方法发现a.getB()是null，就会单独发送关联查询B对象的sql,然后调用a.setB(b)，接着完成a.getB().getName()方法的调用。
```

######     3、Mybatis都有哪些Executor执行器？它们之间的区别是什么？

```yml
##SimpleExecutor 简单执行器
默认，每次执行就开启一个Statement对象，用完就关闭。
##BatchExecutor 批量执行器
将所有sql都添加到批处理中addBatch(),等待统一执行executeBatch(),缓存了多个Statement对象，每个Statement对象都是addBatch()后，等待逐一执行executeBatch()批处理，与JDBC批处理相同。
##ReuseExecutor 重复使用执行器
执行时，以sql作为key查找Statement对象是否存在，存在就是使用，不存在就创建，用完后，不关闭，放入Map中，供下一次使用。
```

###### 4、简述下Mybatis的一级、二级缓存(分别从存储结构、范围、失效场景。三个方面来作答)？

```yml
##一级缓存
指的是sqlSession,用HashMap进行存储，key是由：标签id、参数、分页信息、sql组成，
                             value：就是查询出的结果对象
调用sqlSession的close、clearCache、或执行insert、delete、update，都会清空一级缓存
##二级缓存
指的是同一个namespace下的mapper,多个sqlSession可以共享这个namespace的二级缓存
PerpetualCache是mybatis二级缓存的默认实现，可以看到二级缓存默认也是用hashmap存储，二级缓存的类需要实现Serializable，因为二级缓存有可能会存到硬盘，获取时需要反序列化。
执行了insert、delete、update后commit，都会清空二级缓存(防止脏读)
```

###### 5、简述Mybatis的插件运行原理，以及如何编写一个插件？

```java
//运行原理
1、在mybatis初始化时，创建四大对象时(Executor、StatementHandler、ParameterHandler、ResultHandler)，就会创建拦截器链interceptorChain
2、调用拦截器链pluginAll
3、在pluginAll方法中，调用interceptor.plugin(target)
4、最后返回被拦截器拦截、增强后的对象(target)
//编写一个插件

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.plugin.*;
import java.sql.Connection;
import java.util.Properties;
@Intercepts({
        @Signature(type= StatementHandler.class,//想要拦截的接口
                  method = "prepare",//想要拦截的方法名称
                  args = {Connection.class,Integer.class})//想要拦截的方法的入参类型列表
        ,@Signature(type = Executor.class,method = "",args = {})//如果想要拦截多个接口、或多个方法，可以配置多个
})
public class MyPluginTest implements Interceptor {
   //拦截方法：只要被拦截的目标对象的目标方法被执行时，每次都会执行intercept方法
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("对原方法进行了前置增强....");
        Object proceed = invocation.proceed();//原方法执行
        System.out.println("对原方法进行了后置增强....");
        return proceed;
    }
     //主要为了把当前的拦截器生成代理存到拦截器链中
    @Override
    public Object plugin(Object target) {
        Object wrap = Plugin.wrap(target, this);
        return wrap;
    }
 //获取配置文件的参数
    @Override
    public void setProperties(Properties properties) {
        System.out.println("获取到的配置文件的参数是："+properties);
    }
} 
```