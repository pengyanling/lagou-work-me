package com.lagou.edu.controller;

import com.lagou.edu.bean.UpLoadResult;
import com.lagou.edu.service.FileUpLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/oss")
public class UploadController {
    @Autowired
    private FileUpLoadService fileUpLoadService;
    @PostMapping("/upload")
    @ResponseBody
    public UpLoadResult upload(@RequestParam("file") MultipartFile  multipartFile){
        return  fileUpLoadService.upload(multipartFile);
    }
    @DeleteMapping("/delete/{filePath}")
    @ResponseBody
    public UpLoadResult delete(@PathVariable("filePath") String filePath){
        return  fileUpLoadService.delete(filePath);
    }
    /**
     *  根据文件名进行下载
     * @param objectName
     * @param response
     * @throws IOException
     */
    @RequestMapping("/download/{filePath}")
    @ResponseBody
    public void download(@PathVariable("filePath") String objectName,
                         HttpServletResponse response) throws IOException {
        //通知浏览器以附件形式下载
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(objectName.getBytes(), "ISO-8859-1"));
        fileUpLoadService.exportOssFile(response.getOutputStream(),objectName);
    }

}
