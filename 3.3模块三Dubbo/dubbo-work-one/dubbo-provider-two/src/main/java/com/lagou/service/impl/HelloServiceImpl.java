package com.lagou.service.impl;

import org.apache.dubbo.config.annotation.Service;
import com.lagou.service.HelloService;

@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String name, int timeToWait) {

        return "provider-two:"+name;
    }
}
