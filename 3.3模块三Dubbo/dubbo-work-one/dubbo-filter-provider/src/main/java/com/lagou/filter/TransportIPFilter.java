package com.lagou.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 17:08.
 */

@Activate(group = {CommonConstants.PROVIDER})
public class TransportIPFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String clientIp = RpcContext.getContext().getAttachment("clientIp");
        System.out.println("客户端请求IP：" + clientIp);
        // 执行方法
        return invoker.invoke(invocation);
    }
}
