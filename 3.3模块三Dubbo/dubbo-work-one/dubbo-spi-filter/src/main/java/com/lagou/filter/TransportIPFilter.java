package com.lagou.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 17:08.
 */

@Activate(group = {CommonConstants.CONSUMER})
public class TransportIPFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String ip = invoker.getUrl().getIp();
        String localHost = RpcContext.getContext().getLocalHost();
        String clientIp = RpcContext.getContext().getRemoteHost();
        System.out.println("ip：" + ip + "localHost：" + localHost + "clientIp：" + clientIp);
        RpcContext.getContext().setAttachment("clientIp", ip);
        // 执行方法
        return invoker.invoke(invocation);
    }
}
