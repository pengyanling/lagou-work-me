package com.lagou.controller;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.lagou.service.HelloService;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Reference
    private HelloService helloService;
    @RequestMapping("/toLogin")
    @ResponseBody
    public String login(HttpServletRequest request, HttpServletResponse response) {
        String providerResponse = helloService.sayHello("web request", 100);
        System.out.println(providerResponse);
        return providerResponse;
    }
}
