package com.lagou.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 20:10.
 */
@Configuration
@PropertySource("classpath:/dubbo-consumer.properties")
@ComponentScan("com.lagou.controller")
@EnableDubbo
public class ConsumerConfiguration {
}
