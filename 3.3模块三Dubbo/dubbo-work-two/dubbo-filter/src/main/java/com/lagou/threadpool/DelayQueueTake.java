package com.lagou.threadpool;

import com.lagou.bean.Item;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author pengyanling
 * @createTime 2020年08月13日 23:13.
 */
public class DelayQueueTake implements Runnable {
    private String methodName;

    public DelayQueueTake(String methodName) {
        this.methodName = methodName;
        // 每秒检查队列，移除超过一分钟的元素
        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(this, 5, 1, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        try {
            DelayQueue<Item> items = WachingThreadPool.responseTimeMap.get(methodName);
            Item take = items.take();//移除超过一分钟的元素
            System.out.println(take);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
