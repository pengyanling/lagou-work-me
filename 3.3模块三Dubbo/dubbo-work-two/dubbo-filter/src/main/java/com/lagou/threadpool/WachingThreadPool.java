package com.lagou.threadpool;

import com.lagou.bean.Item;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.common.threadpool.support.fixed.FixedThreadPool;
import org.apache.dubbo.rpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;

@Activate(group = {CommonConstants.CONSUMER})
public class WachingThreadPool extends FixedThreadPool implements Runnable, Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(WachingThreadPool.class);
    // 定义线程池使用的阀值
    public static final Map<String, DelayQueue<Item>> responseTimeMap = new ConcurrentHashMap<>();
    private static final Map<URL, ThreadPoolExecutor> THREAD_POOLS = new ConcurrentHashMap<>();

    public WachingThreadPool() {
        // 每隔5秒打印线程使用情况
        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(this, 1, 5, TimeUnit.SECONDS);
    }

    // 通过父类创建线程池
    @Override
    public Executor getExecutor(URL url) {
        final Executor executor = super.getExecutor(url);
        if (executor instanceof ThreadPoolExecutor) {
            THREAD_POOLS.put(url, (ThreadPoolExecutor) executor);
            responseTimeMap.put(url.getPath(), new DelayQueue<>());
        }
        return executor;
    }

    /**
     * TP90的计算方法也十分简单，介绍如下：
     * <p>
     *      * 1，把一段时间内所有的请求的响应时间，从小到大排序，得到序列A。
     *      * <p>
     *      * 2，总的请求数量，乘以90%，得到90%对应的请求个数C。
     *      * <p>
     *      * 3，从序列A中找到第C个请求，它的响应时间，即为TP90的值
     */
    @Override
    public void run() {
        System.out.println("每隔5秒打印");
        for (String interfaceFun : responseTimeMap.keySet()) {
            DelayQueue<Item> items = responseTimeMap.get(interfaceFun);
            List<Long> responseTimeList = new ArrayList<>(items.size());
            for (Item item : items) {
                responseTimeList.add(item.getResponseTime());
            }
            Collections.sort(responseTimeList, new Comparator<Long>() {
                @Override
                public int compare(Long u1, Long u2) {
                    return Long.compare(u1, u2);
                }
            });
            int size = responseTimeList.size();
            Double tp90 = size * 0.9;
            Double tp99 = size * 0.99;
            LOGGER.error(interfaceFun + "执行了{}次，TP90={}毫秒，TP99={}毫秒。", size,
                    responseTimeList.get(tp90.intValue()), responseTimeList.get(tp99.intValue()));
        }
    }

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {

//        String key = invoker.getInterface().getName() + "." + invocation.getMethodName();
        String key = invocation.getMethodName();
        long startTime = System.currentTimeMillis();
        try {
            // 执行方法
            return invoker.invoke(invocation);
        } finally {
            DelayQueue<Item> items = responseTimeMap.get(key);
            items.put(new Item(System.currentTimeMillis() - startTime));
        }
    }
}
