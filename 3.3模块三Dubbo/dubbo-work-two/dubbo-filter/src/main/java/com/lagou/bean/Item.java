package com.lagou.bean;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 队列元素
 *
 * @author pengyanling
 * @createTime 2020年08月13日 21:59.
 */
public class Item implements Delayed {
    /* 触发时间*/
    private long startTime;
    private long responseTime;
    // 延迟时长，这个是必须的属性因为要按照这个判断延时时长。
    private long excuteTime = 6 * 1000;//一分钟

    public Item(long responseTime) {
        this.startTime = TimeUnit.NANOSECONDS.convert(excuteTime, TimeUnit.MILLISECONDS) + System.nanoTime();
        this.responseTime = responseTime;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(startTime - System.nanoTime(), TimeUnit.NANOSECONDS);
    }

    // 自定义实现比较方法返回 1 0 -1三个参数
    @Override
    public int compareTo(Delayed o) {
        Item item = (Item) o;
        return responseTime > item.responseTime ? 1
                : (responseTime < item.responseTime ? -1 : 0);
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    @Override
    public String toString() {
        return "Item{" +
                "startTime=" + startTime +
                ", responseTime='" + responseTime + '\'' +
                '}';
    }
}
