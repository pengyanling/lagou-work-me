package com.lagou.bean;

import com.lagou.service.MonitorTestService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class ConsumerComponent {

    @Reference
    private MonitorTestService monitorTestService;

    public String methodA() {
//        return monitorTestService.methodA("aa", 0);
        return monitorTestService.methodA("aa", new Random().nextInt(100) + 1);
    }

    public String methodB() {
        return monitorTestService.methodB("bb", new Random().nextInt(100) + 1);
    }
    public String methodC() {
        return monitorTestService.methodC("cc", new Random().nextInt(100) + 1);
    }
}
