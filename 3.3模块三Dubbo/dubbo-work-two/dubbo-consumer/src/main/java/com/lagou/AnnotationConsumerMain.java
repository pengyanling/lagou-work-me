package com.lagou;

import com.lagou.bean.ConsumerComponent;
import com.lagou.threadpool.DelayQueueTake;
import com.lagou.threadpool.WachingThreadPool;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 22:26.
 */
public class AnnotationConsumerMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        final ConsumerComponent service = context.getBean(ConsumerComponent.class);
        WachingThreadPool cacheExecutor = new WachingThreadPool();
        String urlA = "test://localhost/methodA?monitor.test.service=monitorTest";
        String urlB = "test://localhost/methodB?monitor.test.service=monitorTest";
        String urlC = "test://localhost/methodC?monitor.test.service=monitorTest";
        Executor executorA = cacheExecutor.getExecutor(URL.valueOf(urlA));
        Executor executorB = cacheExecutor.getExecutor(URL.valueOf(urlB));
        Executor executorC = cacheExecutor.getExecutor(URL.valueOf(urlC));
        new DelayQueueTake("methodA");
        new DelayQueueTake("methodB");
        new DelayQueueTake("methodC");
        for (int i = 0; i < 3000; i++) {
            executorA.execute(new Runnable() {
                @Override
                public void run() {
                    service.methodA();
                }
            });
            TimeUnit.MICROSECONDS.sleep(1);
            executorB.execute(new Runnable() {
                @Override
                public void run() {
                    service.methodB();
                }
            });
            TimeUnit.MICROSECONDS.sleep(1);
            executorC.execute(new Runnable() {
                @Override
                public void run() {
                    service.methodC();
                }
            });
        }
    }

    @Configuration
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan("com.lagou.bean")
    @EnableDubbo
    static class ConsumerConfiguration {

    }
}
