package com.lagou;

import com.lagou.bean.ConsumerComponent;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.Future;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 22:26.
 */
public class TestConsumerMain {
    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        final ConsumerComponent service = context.getBean(ConsumerComponent.class);
        service.methodA();
        System.out.println(service.methodA());
        Future<Object> future = RpcContext.getContext().getFuture();
        System.out.println("result========"+future.get());
//        System.out.println(service.methodB());
//        System.out.println(service.methodC());
    }

    @Configuration
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan("com.lagou.bean")
    @EnableDubbo
    static class ConsumerConfiguration {

    }
}
