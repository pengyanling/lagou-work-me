package com.lagou.service;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 21:33.
 */
public interface MonitorTestService {
    String methodA(String name, int timeToWait);
    String methodB(String name, int timeToWait);
    String methodC(String name, int timeToWait);
}
