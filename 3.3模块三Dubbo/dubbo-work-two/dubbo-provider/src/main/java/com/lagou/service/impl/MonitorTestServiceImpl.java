package com.lagou.service.impl;

import com.lagou.service.MonitorTestService;
import org.apache.dubbo.config.annotation.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 21:37.
 */
@Service
public class MonitorTestServiceImpl implements MonitorTestService {
    @Override
    public String methodA(String name, int timeToWait) {
        System.out.println("method-AAAAAAAAAAAA：" + name + timeToWait);
        try {
            TimeUnit.MICROSECONDS.sleep(timeToWait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "method-AAAAAAAAAAAA";
    }

    @Override
    public String methodB(String name, int timeToWait) {
        System.out.println("method-BBBBBBBBBBBBB：" + name + timeToWait);
        try {
            TimeUnit.MICROSECONDS.sleep(timeToWait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "method-BBBBBBBBBBBBB";
    }

    @Override
    public String methodC(String name, int timeToWait) {
        System.out.println("method-CCCCCCCCCCC：" + name + timeToWait);
        try {
            TimeUnit.MICROSECONDS.sleep(timeToWait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "method-CCCCCCCCCCC";
    }
}
