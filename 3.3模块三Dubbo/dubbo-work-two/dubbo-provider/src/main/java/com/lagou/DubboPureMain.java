package com.lagou;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author pengyanling
 * @createTime 2020年08月11日 22:51.
 */
public class DubboPureMain {
    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProviderConfiguration.class);
        context.start();

        System.in.read();

//        ClassPathXmlApplicationContext applicationContext  = new ClassPathXmlApplicationContext("classpath:/dubbo-provider.xml");
//        applicationContext.start();
//        System.in.read();
    }

    @Configuration
    @EnableDubbo(scanBasePackages = "com.lagou.service.impl")
    @PropertySource("classpath:/application.properties")
    static class ProviderConfiguration {
    }
}
