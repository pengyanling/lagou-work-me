package com.lagou.hbase.weibo;

import java.io.IOException;

/**
 * @author pengyanling
 * @createTime 2020年10月13日 13:50.
 */
public class TestMain {

    public static void main(String[] args) throws IOException {
        Weibo weibo = new Weibo();
        weibo.init();

        weibo.testPublishContent(weibo);
        weibo.testAddAttend(weibo);
        weibo.testShowMessage(weibo);
        weibo.testRemoveAttend(weibo);
        weibo.testShowMessage(weibo);
    }
}
