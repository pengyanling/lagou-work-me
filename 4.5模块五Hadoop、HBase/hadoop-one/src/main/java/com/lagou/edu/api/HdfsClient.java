package com.lagou.edu.api;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author pengyanling
 * @createTime 2020年10月14日 14:37.
 */
public class HdfsClient {
    @Test
    public void testMkdirs() throws IOException, InterruptedException,
            URISyntaxException {
// 1 获取文件系统
        Configuration configuration = new Configuration();
        FileSystem fs = FileSystem.get(new URI("hdfs://linux127:9000"),
                configuration, "root");
        // 2 创建目录
        fs.mkdirs(new Path("/test_client"));
// 3 关闭资源
        fs.close();
    }
}
