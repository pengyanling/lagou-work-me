package com.lagou.edu.wc;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

//需求：单词计数
//1 继承Mapper类
//2 Mapper类的泛型参数：共4个，2对kv
//2.1 第一对kv:map输入参数类型
//2.2 第二队kv：map输出参数类型
public class WordSortMapper extends Mapper<LongWritable, Text, LongWritable, LongWritable> {
    //3 重写Mapper类的map方法
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        1 接收到文本内容，转为String类型
        final String line = value.toString();
        context.write(new LongWritable(Integer.parseInt(line)), new LongWritable(1));

    }
}
