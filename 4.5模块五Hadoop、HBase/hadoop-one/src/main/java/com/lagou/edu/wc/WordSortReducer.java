package com.lagou.edu.wc;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

//继承的Reducer类有四个泛型参数,2对kv
//第一对kv:类型要与Mapper输出类型一致
//第二队kv:自己设计决定输出的结果数据是什么类型
public class WordSortReducer extends Reducer<LongWritable, LongWritable, LongWritable, LongWritable> {
    //1 重写reduce方法
    private static LongWritable linesum = new LongWritable(1);

    @Override
    protected void reduce(LongWritable key, Iterable<LongWritable> values, Context context)
            throws IOException, InterruptedException {
        for (LongWritable v : values) {
            context.write(linesum, key);
            linesum = new LongWritable(linesum.get()+1);
        }
    }


}
