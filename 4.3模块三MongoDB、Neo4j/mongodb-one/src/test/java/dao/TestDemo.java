package dao;

import com.lagou.edu.bean.Resume;
import com.lagou.edu.repository.ResumeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApplication.class)
public class TestDemo {

    @Resource
    private ResumeRepository resumeRepository;

    @Test
    public void testAdd() {
        for (int i = 1; i <= 20; i++) {
            resumeRepository.save(new Resume(i + "", "test" + i,
                    "cd" + i, new Date(), i*10000));
        }
    }

    @Test
    public void testLoad() {
        List<Resume> list = resumeRepository.findAll();
        System.err.println("=======================================================");
        list.forEach(one -> {
            System.out.println(one);
        });
    }

    @Test
    public void testDelAll() {
        resumeRepository.deleteAll();
    }

}
