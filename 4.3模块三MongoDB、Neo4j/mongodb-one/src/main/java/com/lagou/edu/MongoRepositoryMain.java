package com.lagou.edu;

import com.lagou.edu.bean.Resume;
import com.lagou.edu.repository.ResumeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class MongoRepositoryMain {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(MongoRepositoryMain.class, args);
        ResumeRepository resumeRepository = applicationContext.getBean(ResumeRepository.class);
        //增
        for (int i = 1; i <= 20; i++) {
            resumeRepository.save(new Resume(i + "", "test" + i,
                    "cd" + i, new Date(), i * 10000));
        }
        //查
        List<Resume> list = resumeRepository.findAll();
        System.err.println("=======================================================");
        list.forEach(one -> {
            System.out.println(one);
        });
        //删除
//        resumeRepository.deleteAll();
    }
}
