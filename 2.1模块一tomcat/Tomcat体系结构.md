###### 1、Tomcat整体结构

![image-20200718100723430](../../images/image-20200718100723430.png)

```properties
Server: 表示整个Catalina Servlet容器
	#负责组装并启动Servlet引擎、Tomcat连接器
Service: 是Server内部的组件，一个Server可以包含多个Service
	#它将若干个Connector组件绑定到一个Container
Tomcat的两个核心组件: Connector、Container
```

**1.1、Connector**

```properties
EndPoint: 是Socket接收、发送的处理器
	#用来实现TCP/IP协议
Processor: 读取字节流，解析成Tomcat Request、Response对象
	#用来实现HTTP协议
ProtocolHandler: 是协议的接口
	#通过Endpoint、Processor，实现针对具体协议的处理能力
CoyoteAdapter: 将Tomcat的Request对象转为ServletRequest对象，传给容器(Container)
```

**1.2、Container**

```properties
Engine: 表示整个Catalina的Servlet引擎，用来管理多个虚拟站点
	#一个Service最多只能有一个Engine
Host: 代表一个虚拟主机【或者说一个站点】
	#可以给Tomcat配置多个虚拟主机地址
	#一个Engine可以有多个Host
Context: 表示一个Web应用程序
	#一个虚拟主机下，可以包含多个Context
Wrapper: 表示一个Servlet
	# Warpper作为容器中的最底层，不能包含子容器
	#一个Context可以包含多个Warpper
```

###### 2、Tomcat启动流程

![image-20200717172046969](../../images/image-20200717172046969.png)

###### 3、Tomcat请求处理流程.png

![Tomcat请求处理流程.png](../../images/Tomcat请求处理流程.png)