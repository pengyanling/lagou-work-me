package server;

import server.mapper.Mapper;
import server.mapper.Wapper;

import java.io.InputStream;
import java.net.Socket;
import java.util.List;

public class RequestProcessor extends Thread {

    private Socket socket;
    private List<Mapper> mapperList;

    public RequestProcessor(Socket socket, List<Mapper> mapperList) {
        this.socket = socket;
        this.mapperList = mapperList;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());
            //真实的请求： http://localhost:8080/demo1/lagou
            String url = request.getUrl();//封装到request后的url: /demo1/lagou
            String[] split = url.split("/");
            String appName = "";
            String urlPattern = "";
            if (split.length == 3) {
                appName = split[1];
                urlPattern = "/" + split[2];//配置的urlPattern是带斜杠的
            }
            Mapper mapperIn = new Mapper(appName);
            Wapper wapperIn = new Wapper(urlPattern);
            boolean exists = false;
            for (Mapper mapper : mapperList) {
                if (mapper.equals(mapperIn)) {
                    //是这个应用
                    for (Wapper wapper : mapper.getWapperList()) {
                        if (wapper.equals(wapperIn)) {
                            // 动态资源servlet请求
                            wapper.getServlet().service(request, response);
                        } else {
                            // 静态资源处理
                            response.outputHtml(mapper.getAppBasePath()+request.getUrl());

                        }
                    }
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                //没有找到这个应用
                response.outputHtml(request.getUrl());
            }
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
