package server;

public abstract class HttpServlet implements Servlet{

    protected String testField;

    public String getTestField() {
        return testField;
    }

    public void setTestField(String testField) {
        this.testField = testField;
    }

    public abstract void doGet(Request request, Response response);

    public abstract void doPost(Request request,Response response);

    @Override
    public void init() throws Exception {
        testField="Http";
        System.out.println("HttpServlet init");

    }

    @Override
    public void service(Request request, Response response) throws Exception {
        if("GET".equalsIgnoreCase(request.getMethod())) {
            doGet(request,response);
        }else{
            doPost(request,response);
        }
    }
}
