package server;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import server.loader.MyClassLoader;
import server.mapper.Mapper;
import server.mapper.Wapper;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Minicat的主类
 */
public class Bootstrap {

    /**
     * 定义socket监听的端口号
     */
    private int port;

    private List<Mapper> mapperList=new ArrayList<>();

    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {

        loadServer();


        // 定义一个线程池
        int corePoolSize = 10;
        int maximumPoolSize = 50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();


        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler
        );
        ServerSocket serverSocket = new ServerSocket(port);

        System.out.println("=========>>>>>>使用线程池进行多线程改造");
        /*
            多线程改造（使用线程池）
         */
        while (true) {

            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket, mapperList);
            //requestProcessor.start();
            threadPoolExecutor.execute(requestProcessor);
        }


    }


    /**
     * 加载解析server.xml
     */
    private void loadServer() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();

        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> connectorNodes = rootElement.selectNodes("//Connector");
            port = Integer.valueOf(connectorNodes.get(0).attributeValue("port"));//设置端口

            List<Element> hostNodes = rootElement.selectNodes("//Host");
            String hostName = hostNodes.get(0).attributeValue("name");
            String appBase = hostNodes.get(0).attributeValue("appBase");
            File webappsDir = new File(appBase);
            File[] apps = webappsDir.listFiles();
            for (File app : apps) {
                String appName = app.getName();
                String path = app.getPath();
                // 加载解析相关的配置，web.xml
                List<Wapper> wapperList = loadServlet(path);
                //这个appBase不带应用名称，因为请求的url带了应用名称的
                mapperList.add(new Mapper(appBase,appName,wapperList));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 加载解析web.xml，初始化Servlet
     */
    private List<Wapper> loadServlet(String appPath) throws FileNotFoundException {
        InputStream resourceAsStream = new FileInputStream(new File(appPath + "/web.xml"));
        SAXReader saxReader = new SAXReader();
        List<Wapper> wapperList = new ArrayList<>();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//servlet");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element = selectNodes.get(i);
                // <servlet-name>lagou</servlet-name>
                Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletnameElement.getStringValue();
                // <servlet-class>server.LagouServlet</servlet-class>
                Element servletclassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletclassElement.getStringValue();


                // 根据servlet-name的值找到url-pattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // /lagou
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                // server.LagouServlet.class
                MyClassLoader myClassLoader = new MyClassLoader(appPath+"/"+servletClass.replace('.','/')+".class");

                //加载servlet这个class文件
                Class<?> servletClazz = myClassLoader.loadClass(servletClass);
                HttpServlet servlet = (HttpServlet)servletClazz.newInstance();
                servlet.init();//测试是否调用了正确的servlet
                wapperList.add(new Wapper(urlPattern, servlet));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return wapperList;
    }


    /**
     * Minicat 的程序启动入口
     *
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 启动Minicat
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
