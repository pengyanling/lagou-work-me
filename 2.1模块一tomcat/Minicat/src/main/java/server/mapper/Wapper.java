package server.mapper;

import server.HttpServlet;

/**
 * @author pengyanling
 * @createTime 2020年07月18日 11:50.
 */
public class Wapper {
    private String urlPattern;
    private HttpServlet servlet;

    public Wapper() {
    }

    public Wapper(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public Wapper(String urlPattern, HttpServlet servlet) {
        this.urlPattern = urlPattern;
        this.servlet = servlet;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            Wapper o = (Wapper) obj;
            return o.getUrlPattern().equals(urlPattern) ? true : false;
        }
        return false;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public HttpServlet getServlet() {
        return servlet;
    }

    public void setServlet(HttpServlet servlet) {
        this.servlet = servlet;
    }
}
