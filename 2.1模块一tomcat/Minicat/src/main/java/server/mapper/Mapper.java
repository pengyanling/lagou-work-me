package server.mapper;

import java.util.List;

/**
 * @author pengyanling
 * @createTime 2020年07月18日 11:15.
 */
public class Mapper {
//    private String host;//localhost在request中没有封装，所以我这里也不要host了

//这个appBasePath不带应用名称，因为请求的url带了应用名称的
    private String appBasePath;
    private String context;
    private List<Wapper> wapperList;

    public Mapper() {
    }

    public Mapper( String context) {
        this.context = context;
    }
    public Mapper(String appBasePath, String context, List<Wapper> wapperList) {
        this.appBasePath=appBasePath;
        this.context = context;
        this.wapperList = wapperList;
    }


    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public List<Wapper> getWapperList() {
        return wapperList;
    }

    public void setWapperList(List<Wapper> wapperList) {
        this.wapperList = wapperList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            Mapper o =(Mapper)obj;
            return o.getContext().equals(context) ? true : false;
        }
        return false;
    }

    public String getAppBasePath() {
        return appBasePath;
    }

    public void setAppBasePath(String appBasePath) {
        this.appBasePath = appBasePath;
    }
}
