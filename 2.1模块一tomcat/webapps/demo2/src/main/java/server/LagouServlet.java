package server;

import java.io.IOException;

/**
 * @author pengyanling
 * @createTime 2020年07月18日 11:01.
 */
public class LagouServlet extends HttpServlet {
    @Override
    public void doGet(Request request, Response response) {
        System.out.println("Demo2 LagouServlet get");

        String content = "<h1>Demo2 LagouServlet get</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        System.out.println("Demo2 LagouServlet get");

        String content = "<h1>Demo2 LagouServlet post</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {
        testField="Demo two";
        System.out.println("Demo2 LagouServlet init");

    }

    @Override
    public void destory() throws Exception {

    }
}
