/*
创建数据库bank
*/
create database `bank` character set utf8 collate utf8_general_ci;
SET FOREIGN_KEY_CHECKS=0;
/*
使用数据库bank
*/
use bank;

-- ----------------------------
-- 先删表，再建表
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `cardNo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `money` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 向account表插入2条数据
-- ----------------------------
INSERT INTO `account` VALUES ('6029621011001', '韩梅梅',1000);
INSERT INTO `account` VALUES ('6029621011000', '李大雷',1000);
