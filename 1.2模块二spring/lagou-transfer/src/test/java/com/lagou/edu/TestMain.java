package com.lagou.edu;

import com.lagou.edu.dao.AccountDao;
import com.lagou.edu.factory.BeanFactory;
import com.lagou.edu.service.TransferService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author pengyanling
 * @createTime 2020年07月01日 9:32.
 */
public class TestMain {
    private AccountDao accountDao;
    private TransferService transferService;

    @Before
    public void before() throws Exception {
        accountDao = (AccountDao) BeanFactory.getBean("accountDao");
        transferService = (TransferService) BeanFactory.getBean("transferService");
        System.err.println("转账前：");
        System.out.println(accountDao.queryAccountByCardNo("6029621011000"));
        System.out.println(accountDao.queryAccountByCardNo("6029621011001"));

    }
    @After
    public void after()throws Exception{
        System.err.println("转账后：");
        System.out.println(accountDao.queryAccountByCardNo("6029621011000"));
        System.out.println(accountDao.queryAccountByCardNo("6029621011001"));

    }

    /**
     * 转账成功
     * @throws Exception
     */
    @Test
    public void testSuccess() throws Exception {
        transferService.transferSuccess("6029621011000", "6029621011001", 100);
    }

    /**
     * 转账异常，事务回滚
     * @throws Exception
     */
    @Test
    public void testError() throws Exception {
        transferService.transferError("6029621011000", "6029621011001", 100);
    }
    /**
     * 转账异常，没有事务控制
     * @throws Exception
     */
    @Test
    public void test() throws Exception {
        transferService.transfer("6029621011000", "6029621011001", 100);
    }
}
