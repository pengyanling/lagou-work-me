package com.lagou.edu.factory;

import com.lagou.edu.annotation.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/**
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {

    /**
     * 任务一：读取解析xml
     * 任务二：扫描包，获取包下的所有类，判断是否有注解
     * 通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String, Object> map = new HashMap<>();  // 存储对象


    static {
        // 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
        // 加载xml
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        // 解析xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            scanClassAnnotation(rootElement);
            setAutowired();
            haveTransactionalNewProxy();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

    }

    /**
     * 扫描类注解
     *
     * @param rootElement
     */
    private static void scanClassAnnotation(Element rootElement) throws IllegalAccessException, InstantiationException {
        Element componentScan = rootElement.element("component-scan");//注解扫描
        String basePackage = componentScan.attributeValue("base-package");// com.lagou.edu

        List<Class<?>> classes = getClasses(basePackage); //获取包下所有类
        //如果类上有Service注解，将该类的信息，添加到map集合
        for (Class<?> classInfo : classes) {
            Service service = classInfo.getAnnotation(Service.class);
            Repository repository = classInfo.getAnnotation(Repository.class);
            Configuration configuration = classInfo.getAnnotation(Configuration.class);
            if (service != null) {
                String id = service.value();
                if (id == null || "".equals(id.trim())) {
                    id = toLowerCaseFirstOne(classInfo.getInterfaces()[0].getSimpleName());//获取接口的名字作为id
                }
                map.put(id, classInfo.newInstance()); // 实例化之后的对象
            } else if (repository != null) {
                String id = repository.value();
                if (id == null || "".equals(id.trim())) {
                    id = toLowerCaseFirstOne(classInfo.getInterfaces()[0].getSimpleName());//获取接口的名字作为id
                }
                map.put(id, classInfo.newInstance()); // 实例化之后的对象
            } else if (configuration != null) {
                String className = classInfo.getSimpleName();//获取类的名字作为id
                map.put(toLowerCaseFirstOne(className), classInfo.newInstance()); // 实例化之后的对象

            }
        }
    }

    // 首字母转小写
    public static String toLowerCaseFirstOne(String s) {
        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }


    /**
     * 对有Autowired注解的属性赋值
     *
     * @throws IllegalAccessException
     */
    private static void setAutowired() throws IllegalAccessException {
        for (String id : map.keySet()) {
            Object object = map.get(id);
            Class<? extends Object> classInfo = object.getClass();
            Field[] fields = classInfo.getDeclaredFields();
            for (Field field : fields) {
                Autowired autowired = field.getAnnotation(Autowired.class);
                if (autowired != null) {
                    Object bean = getBean(field.getName());//属性名与类名一致，首字母小写
                    if (bean != null) {
                        // 3.默认使用属性名称，查找bean容器对象 1参数 当前对象 2参数给属性赋值
                        field.setAccessible(true); // 允许访问私有属性
                        field.set(object, bean);
                    }
                }
            }
        }
    }

    /**
     * 如果有Transactional注解，就生成代理对象
     */
    private static void haveTransactionalNewProxy() {
        for (String id : map.keySet()) {
            Object object = map.get(id);
            Class<? extends Object> classInfo = object.getClass();
            Method[] methods = classInfo.getDeclaredMethods();
            ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
            for (Method method : methods) {
                Transactional transactional = method.getAnnotation(Transactional.class);
                if (transactional != null) {
                    Class<?>[] interfaces = classInfo.getInterfaces();
                    if (interfaces != null && interfaces.length > 0) {//如果实现了接口就用Jdk动态代理生成代理对象
                        object = proxyFactory.getJdkProxy(object);
                    } else {//否则用cglib动态代理生成代理对象
                        object = proxyFactory.getCglibProxy(object);
                    }
                    map.put(id, object);//将代理对象放入map中
                    break;//只要有一个方法Transactional注解，就给这个类产生代理对象
                }
            }
        }
    }

    /**
     * 从包中获取所有的Class
     *
     * @param packageName
     * @return
     */
    public static List<Class<?>> getClasses(String packageName) {
        List<Class<?>> classes = new ArrayList<>();
        boolean recursive = true;
        // 获取包的名字 并进行替换
        String packageDirName = packageName.replace('.', '/');

        Enumeration<URL> dirs;
        try {
            dirs = BeanFactory.class.getClassLoader().getResources(packageDirName);
            // 将classes、test-classes下的包都加载
            while (dirs.hasMoreElements()) {
                URL url = dirs.nextElement();
                String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                scanFile(packageName, filePath, recursive, classes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }

    /**
     * 以文件的形式来获取包下的所有Class
     *
     * @param packageName
     * @param packagePath
     * @param recursive
     * @param classes
     */
    public static void scanFile(String packageName, String packagePath, final boolean recursive,
                                List<Class<?>> classes) {
        File dir = new File(packagePath);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }

        File[] dirfiles = dir.listFiles(new FileFilter() {
            //查找以.class结尾的文件
            // 是目录就循环子包
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        for (File file : dirfiles) {
            if (file.isDirectory()) {// 是目录
                scanFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive,
                        classes);
            } else {
                // 是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    // 添加到集合中去
                    classes.add(Class.forName(packageName + '.' + className));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static Object getBean(String id) {
        return map.get(id);
    }
}
