package com.lagou.edu.factory;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Configuration;
import com.lagou.edu.annotation.Transactional;
import com.lagou.edu.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @author 应癫
 * <p>
 * <p>
 * 代理对象工厂：生成代理对象的
 */

@Configuration
public class ProxyFactory {

    @Autowired
    private TransactionManager transactionManager;
    /**
     * Jdk动态代理
     *
     * @param obj 委托对象
     * @return 代理对象
     */
    public Object getJdkProxy(Object obj) {

        // 获取代理对象
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        //入参method是接口的方法，取不到注解，要在实现类obj查找是否存在注解
                        Annotation[] annotations = obj.getClass().getMethod(method.getName(), method.getParameterTypes()).getAnnotations();
                        boolean hasAnnotation = Arrays.stream(annotations).filter(annotation -> {
                            return annotation.annotationType().isAssignableFrom(Transactional.class);
                        }).count() > 0;
                        if(hasAnnotation){//如果有transactional注解才开启事务控制
                            try {
                                // 开启事务(关闭事务的自动提交)
                                transactionManager.beginTransaction();
                                result = method.invoke(obj, args);
                                // 提交事务
                                transactionManager.commit();
                            } catch (Exception e) {
                                // 回滚事务
                                transactionManager.rollback();
                                // 抛出异常便于上层servlet捕获
                                throw e;
                            }
                        }else{//没有transactional注解不做控制
                            result = method.invoke(obj, args);
                        }

                        return result;
                    }
                });

    }


    /**
     * 使用cglib动态代理生成代理对象
     *
     * @param obj 委托对象
     * @return
     */
    public Object getCglibProxy(Object obj) {
        return Enhancer.create(obj.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                Object result = null;
                Transactional transactional = method.getAnnotation(Transactional.class);
                if(transactional!=null) {//如果有transactional注解才开启事务控制
                    try {
                        // 开启事务(关闭事务的自动提交)
                        transactionManager.beginTransaction();
                        result = method.invoke(obj, objects);
                        // 提交事务
                        transactionManager.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        // 回滚事务
                        transactionManager.rollback();
                        // 抛出异常便于上层servlet捕获
                        throw e;
                    }
                }else{
                    result = method.invoke(obj, objects);
                }
                return result;
            }
        });
    }
}
