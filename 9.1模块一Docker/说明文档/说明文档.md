##### 1、环境说明

| IP        | 192.168.0.127 | 192.168.0.129 |
| --------- | ------------- | ------------- |
| swarm角色 | manager       | worker        |

- 两台机器都需要安装docker、docker-compose
- 两台机器都需要**下载swarm镜像**

##### 2、安装docker

###### 2.1、安装docker【两台】

**如果安装过，需要先卸载原来的docker**

```properties
#查看安装
yum list installed | grep docker
#卸载
yum -y remove containerd.io.x86_64
yum -y remove docker-ce.x86_64
yum -y remove docker-ce-cli.x86_64
#删库
rm -rf /var/lib/docker
```

**卸载完后再安装**

```properties
#这里使用脚本自动安装的方式：安装docker
	#先要在/opt目录下创建docker：mkdir docker
1、进入目录: cd /opt/docker
2、下载get-docker.sh文件: curl -fsSL get.docker.com -o get-docker.sh
3、以阿里云镜像安装get-docker.sh脚本内容: sudo sh get-docker.sh --mirror Aliyun
4、启动docker: systemctl start docker
5、查看docker版本: docker --version
	#查看Docker启动信息：docker info
```

![image-20201020103542180](../../../images/image-20201020103542180.png)

```properties
可以启动一个基于hello-world镜像的容器: docker run hello-world
	#测试docker是否安装成功
	能输出下图的内容，表示docker可以使用了
```

![image-20201020104454940](../../../images/image-20201020104454940.png)

###### 2.2、安装docker-compose【两台】

参考文章：[https://blog.csdn.net/pyl574069214/article/details/106669222#%C2%A0%C2%A0%C2%A0%C2%A0%C2%A07%E3%80%81docker-compose](https://blog.csdn.net/pyl574069214/article/details/106669222#   7、docker-compose)

1、docker-compose下载地址：https://github.com/docker/compose/releases/tag/1.24.1

2、下载的文件名：docker-compose-Linux-x86_64

```properties
1、进入目录: cd /usr/local/bin
2、将下载的docker-compose-Linux-x86_64拖拽上传到/usr/local/bin这个目录
3、将文件重命名为docker-compose: mv docker-compose-Linux-x86_64 docker-compose
4、设置文件执行权限: chmod 777 docker-compose
5、配置环境变量: vi /etc/profile
	#在export PATH=后面添加：:/usr/local/bin
	#效果如下图
```

![image-20201020131033445](../../../images/image-20201020131033445.png)

```properties
6、让环境变量配置生效: source /etc/profile
7、在任意目录下运行: docker-compose
	#有输出结果，并且没有报错，就表示docker-compose安装成功了
```

##### 3、安装swarm

###### 3.1、下载镜像【两台】

```properties
1、拉取swarm镜像: docker pull swarm
2、查看镜像: docker images
	#可以看到有一个swarm镜像
3、查看swarm版本: docker run --rm swarm -v
```

###### 3.2、创建集群【127机器】

```properties
1、在manager节点执行: docker swarm init --advertise-addr 192.168.0.127
	#192.168.0.127是manager机器的ip
#执行上面命令后，会输出worker方式加入集群的的token
2、查看集群状态: docker info
3、查看节点信息: docker node ls

#如果已经做过一次了，想重新做，需要执行下面的命令
## 将节点强制驱除集群
docker swarm leave --force
```

```properties
如果忘了token，可以在manager节点上执行下面的命令
1、查看以manager方式加入集群的token: docker swarm join-token manager
2、查看以worker方式加入集群的token: docker swarm join-token worker
```

###### 3.3、加入集群【129机器】

```properties
#将127机器上执行：docker swarm join-token worker命令输出的命令在129机器上执行
	#我这里的命令如下：
1、添加worker节点到集群: docker swarm join --token SWMTKN-1-1azokvkkoslegqx15izbl7j99deol10doq5uugkf1si0z47zkn-cgnrpdcjr5jo2950km7oeysxf 192.168.0.127:2377

#上面命令执行完成后，可以在127机器上查看节点信息：docker node ls，就可以看到129机器已经加入集群

#如果已经做过一次了，想重新做，需要执行下面的命令
## 将节点强制驱除集群
docker swarm leave --force
```

###### 3.4、发布服务到集群

```properties
#在manager节点可执行的查询命令
1、查看发布的服务: docker service ls
2、查看服务详细信息: docker service inspect --pretty nginx1
	#nginx1是服务名称
3、查看哪些节点正在运行服务: docker service ps nginx1
	#nginx1是服务名称
4、停止并删除发布的服务: docker service rm nginx1
	#nginx1是服务名称
5、扩展一个或多个服务: docker service scale nginx1=3
	#nginx1是服务名称
	# 3：表示要将nginx1服务扩展成3个
6、更新服务: docker service update --publish-rm 80:80 --publish-add 88:80 nginx1
	#将nginx1服务从原来的80:80端口删除，在88:80端口发布
还可以更新很多 具体可加 --help查看
```

**发布服务到集群的命令：**docker service create -p 80:80 --replicas 2 --name nginx1 nginx

```properties
#命令参数说明
-p: 端口映射
	# 80:80
--replicas: 运行实例个数
	# 2
--name: 服务名
	# nginx1 
nginx: 镜像
```

##### 4、与Swarm一起使用Compose

1、先创建volumes挂载的目录，如果目录或文件不存在将**启动不成功**，所以要先创建好目录或文件，【**主-备**机，所有机器都需要创建下面的目录】

```properties
1、进入目录: cd /opt/docker
2、创建nginx相关目录1: mkdir nginx/log -p
3、创建nginx相关目录2: mkdir nginx/www -p
4、创建nginx相关目录3: mkdir /etc/letsencrypt -p
	#nginx加密相关文件【https访问时】
5、创建mysql相关目录: mkdir mysql/db -p
6、创建redis相关目录: mkdir redis/data -p
```

###### 2、docker-compose.yml文件内容

```properties
#在127机器执行下面的命令
1、进入目录: cd /opt/docker
2、创建目录: mkdir swarm-com-test
3、进入目录: cd swarm-com-test
4、编辑文件: vim docker-compose.yml
	#文件内容如下
```

```yml
version: "3.4"
services:
  nginx:
    image: nginx:1.18.0
    ports:
      - 80:80
      - 443:443
    volumes:
      - /opt/docker/nginx/log:/var/log/nginx
      - /opt/docker/nginx/www:/etc/nginx/html
      - /etc/letsencrypt:/etc/letsencrypt
    deploy:
      mode: replicated
      replicas: 2
  mysql:
    image: mysql:5.7.30
    ports:
      - 3306:3306
    command:
      --default-authentication-plugin=mysql_native_password
      --character-set-server=utf8mb4
      --collation-server=utf8mb4_general_ci
      --explicit_defaults_for_timestamp=true
      --lower_case_table_names=1
      --default-time-zone=+8:00
    environment:
      MYSQL_ROOT_PASSWORD: "root"
    volumes:
      - "/opt/docker/mysql/db:/var/lib/mysql"
    deploy:
      mode: replicated
      replicas: 2
  redis:
    image: redis:5.0.9
    environment:
      - TZ=Asia/beijing
    ports:
      - 6379:6379
    volumes:
      - /opt/docker/redis/data:/data
    deploy:
      mode: replicated
      replicas: 2
```

```properties
1、运行: docker stack deploy -c docker-compose.yml web
2、查看服务: docker stack services web
	#查看网络：docker network ls
3、删除服务: docker stack down web
```

```properties
#其他命令
1、登录到mysql容器: docker exec -it 9d bash
	#	9d：是mysql容器的id；可以通过docker ps命令查看容器id
进入容器bash后，就可以输入用户名、密码，登录mysql了: mysql -uroot -p
#回车后，输入密码；
```

##### 5、构建hot镜像

1、将test.sql数据库导入到上面以集群方式启动的数据库中【我这里是导入到**127机器**上的】

2、能访问

​	就是调用查询接口，能正常返回数据：curl http://192.168.0.127:8080/login/orders

  *访问jsp页面有问题，出不来*

```properties
#将6.1的作业项目rabbitmq-one的数据库地址改为192.168.0.127，去掉rabbitmq相关的代码、依赖jar包，重新运行重新成功后，能访问，再打成jar包，将jar包重命名为Hot.jar
1、进入目录: /opt/docker/swarm-com-test/test
	#如果目录不存在，需要先创建
2、将Hot.jar拖拽上传
#运行：java -jar Hot.jar，将项目启动起来，看是否能访问，能访问，将这个jar服务停止后，再进行下面的操作。
3、编辑文件: vim dockerfile-hot
	#文件内容如下
```

```properties
FROM daocloud.io/library/java:openjdk-8-alpine
ADD ["Hot.jar","Hot.jar"]
EXPOSE :8080
ENTRYPOINT ["java","-jar","/Hot.jar"]
```

上面命令说明：

第一行：springboot依赖jdk8
第二行：将打好的jar包放入容器，别名也叫Hot.jar
第三行：监听8080端口
第四行：容器运行后执行的命令

```properties
#可以先将jdk8拉取下来：docker pull daocloud.io/library/java:openjdk-8-alpine
#然后再构建镜像
4、构建镜像: docker build -f dockerfile-hot -t lgedu/hot:1.0 .
	# lgedu/hot：是镜像名称
	# 1.0是镜像的版本
5、查看镜像: docker images
6、运行镜像: docker run -d --name hot -p 8080:8080 lgedu/hot:1.0
	#查看正在运行的容器：docker ps
	#终止容器：docker stop 容器id
7、删除镜像: docker rmi lgedu/hot:1.0
	#查看所有容器：docker ps -a
	#删除容器：docker rm 容器id
```

##### 6、将hot镜像放入swarm集群

预先执行命令

```properties
1、终止hot容器
	#终止容器：docker stop 容器id
2、删除swarm服务: docker stack down web
```

**重新修改docker-compose.yml**

```properties
1、进入目录: cd /opt/docker/swarm-com-test
2、编辑文件: vim docker-compose.yml
	#加入hot镜像，添加后的效果
```

```yml
version: "3.4"
services:
  mysql:
    image: mysql:5.7.30
    ports:
      - 3306:3306
    command:
      --default-authentication-plugin=mysql_native_password
      --character-set-server=utf8mb4
      --collation-server=utf8mb4_general_ci
      --explicit_defaults_for_timestamp=true
      --lower_case_table_names=1
      --default-time-zone=+8:00
    environment:
      MYSQL_ROOT_PASSWORD: "root"
    volumes:
      - "/opt/docker/mysql/db:/var/lib/mysql"
    deploy:
      mode: replicated
      replicas: 2
  redis:
    image: redis:5.0.9
    environment:
      - TZ=Asia/beijing
    ports:
      - 6379:6379
    volumes:
      - /opt/docker/redis/data:/data
    deploy:
      mode: replicated
      replicas: 2
  hot:
    image: lgedu/hot:1.0
    ports:
      - 8080:8080
    deploy:
      mode: replicated
      replicas: 2
```

```properties
1、运行: docker stack deploy -c docker-compose.yml web
2、查看服务: docker stack services web
	#查看网络：docker network ls
```

