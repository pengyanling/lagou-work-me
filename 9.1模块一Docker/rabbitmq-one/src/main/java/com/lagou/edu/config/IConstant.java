package com.lagou.edu.config;

/**
 * @author pengyanling
 * @createTime 2020年10月26日 11:38.
 */
public interface IConstant {
    /**
     * 交换器名称-正常：ex.order
     */
    String change_name="ex.order";
    /**
     * 交换器名称-死信：ex.order.dlx
     */
    String change_name_dlx="ex.order.dlx";
    /**
     * 路由键-正常：order
     */
    String routing_Key="order";
    /**
     * 路由键-死信：order.dlx
     */
    String routing_Key_dlx="order.dlx";
    /**
     * 队列名称-正常: queue.order
     */
    String queue_name="queue.order";
    /**
     * 队列名称-死信队列: queue.order.dlx
     */
    String queue_name_dlx="queue.order.dlx";
}
