package com.lagou.edu.service;

import com.lagou.edu.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author pengyanling
 * @createTime 2020年10月26日 14:23.
 */
@Service
public class OrderServiceImpl {

    @Autowired
    private OrderMapper orderMapper;

    /**
     *
     * @param message
     * @param channel
     * @throws IOException
     */
    /*@RabbitListener(queues = queue_name_dlx)
    public void onMessage(Message message, Channel channel) throws IOException {
        String s = new String(message.getBody());
        long id = Long.parseLong(s);
        System.out.println("订单id：" + id + "已取消订单");
        orderMapper.cancelOrder(id);

        // 消息确认【手动补偿】
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }*/
}
