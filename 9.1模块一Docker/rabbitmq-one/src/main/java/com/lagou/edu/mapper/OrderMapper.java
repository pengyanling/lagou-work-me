package com.lagou.edu.mapper;

import com.lagou.edu.pojo.OrderBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:20.
 */
public interface OrderMapper {

    List<OrderBean> findAll();

    int save(OrderBean bean);

    int mod(OrderBean bean);

    void payMoney(@Param("id") Long id);

    int cancelOrder(@Param("id")Long id);
}
