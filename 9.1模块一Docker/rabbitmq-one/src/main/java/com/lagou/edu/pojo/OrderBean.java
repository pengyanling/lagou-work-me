package com.lagou.edu.pojo;

import lombok.Data;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:34.
 */
@Data
public class OrderBean {
    /**
     * 主键
     */
    private int id;
    private String name;
    private double price;
    private int num;
    private String payState;

}
