/*
创建数据库test
*/
create database `test` character set utf8 collate utf8_general_ci;
SET FOREIGN_KEY_CHECKS=0;
/*
使用数据库test
*/
use test;

-- ----------------------------
-- Table structure for good
-- ----------------------------
DROP TABLE IF EXISTS `good`;
CREATE TABLE `good` (
  `commodity` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of good
-- ----------------------------
INSERT INTO `good` VALUES ('1', '10');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `orderId` int(11) NOT NULL,
  `commodity` int(11) DEFAULT NULL,
  `status` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for tb_resume
-- ----------------------------
DROP TABLE IF EXISTS `tb_resume`;
CREATE TABLE `tb_resume` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_resume
-- ----------------------------
INSERT INTO `tb_resume` VALUES ('2', '上海', '李四1', '151000000');
INSERT INTO `tb_resume` VALUES ('3', '广州', '王五', '153000000');
INSERT INTO `tb_resume` VALUES ('11', '成都全文rsg', '李留为', '48511343asd');
INSERT INTO `tb_resume` VALUES ('16', '成都', '李四12324', '23');
INSERT INTO `tb_resume` VALUES ('17', '346', 'er收到', '46757');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `pay_state` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('2', '2', '2.00', '2', '已取消');
INSERT INTO `t_order` VALUES ('1', '1', '1.00', '1', '已支付');
INSERT INTO `t_order` VALUES ('4', '4', '4.00', '4', '已取消');
INSERT INTO `t_order` VALUES ('3', '3', '3.00', '3', '已支付');
INSERT INTO `t_order` VALUES ('5', '5', '5.00', '5', '已支付');
