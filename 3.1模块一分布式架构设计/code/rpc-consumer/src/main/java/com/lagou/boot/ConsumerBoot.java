package com.lagou.boot;

import com.lagou.client.RPCConsumer;
import com.lagou.pojo.RpcRequest;
import com.lagou.service.IUserService;

public class ConsumerBoot {

    //参数定义
    private static final String PROVIDER_NAME = "UserService#sayHello#";

    public static void main(String[] args) throws InterruptedException {
        RpcRequest rpcRequest=new RpcRequest();
        rpcRequest.setClassName("com.lagou.service.UserServiceImpl");

        //1.创建代理对象
        IUserService service = (IUserService) RPCConsumer.createProxy(IUserService.class, rpcRequest);

        //2.循环给服务器写数据
        int i = 1;
        while (true) {
            String result = service.sayHello(" I am Client !!! " + i++);
            System.out.println(result);
            Thread.sleep(2000);
        }

    }
}
