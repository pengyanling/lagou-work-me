package com.lagou.pojo;

/**
 * 通信协议对象
 *
 * @author pengyanling
 * @createTime 2020年07月27日 16:51.
 */
public class RpcRequest {
    private String requestId;//请求对象的Id
    private String className;//类名
    private String methodName;//方法名
    private Class<?>[] parameterTypes;//参数类型

    private Object[] parameters;//入参

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
