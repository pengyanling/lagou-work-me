package com.lagou.demo.controller;

import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author pengyanling
 * @createTime 2020年07月07日 11:25.
 */
@Security({"zhangsan", "lisi", "wangwu"})
@LagouController
@LagouRequestMapping("/sec")
public class SecurityTestController {
    @Security("zhangsan")
    @LagouRequestMapping("/handle1")
    public void handle1(HttpServletRequest request, HttpServletResponse response, String username) {
        try {
            response.getWriter().write("welcome " + username + " login.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Security("lisi")
    @LagouRequestMapping("/handle2")
    public void handle2(HttpServletRequest request, HttpServletResponse response, String username) {
        try {
            response.getWriter().write("welcome " + username + " login.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @LagouRequestMapping("/handle3")
    public void handle3(HttpServletRequest request, HttpServletResponse response, String username) {
        try {
            response.getWriter().write("welcome " + username + " login.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
