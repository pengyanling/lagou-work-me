package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * 控制用户访问Handler方法的权限
 *
 * @author pengyanling
 * @createTime 2020年07月07日 9:17.
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    String[] value();
}
