package com.lagou.edu.pojo;

/**
 * @author pengyanling
 * @createTime 2020年07月09日 18:44.
 */
public class Result {
    String message;

    public Result(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
