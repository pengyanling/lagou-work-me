package com.lagou.codec;

import com.lagou.serial.Serializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 编码器
 * @author pengyanling
 * @createTime 2020年07月27日 17:04.
 */
public class RpcEncoder extends MessageToByteEncoder {
    private Class<?> clazz;
    private Serializer serializer;

    public RpcEncoder(Class<?> clazz, Serializer serializer) {
        this.clazz = clazz;
        this.serializer = serializer;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object msg, ByteBuf byteBuf) throws Exception {
        byte[] bytes =null;
        if(clazz!=null && clazz.isInstance(msg)){
           bytes = serializer.serialize(msg);
        }else{
            bytes = msg.toString().getBytes();
        }
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }
}
