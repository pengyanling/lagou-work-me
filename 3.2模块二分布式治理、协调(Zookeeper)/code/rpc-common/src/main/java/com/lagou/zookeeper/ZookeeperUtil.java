package com.lagou.zookeeper;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

import java.util.List;

/**
 * @author pengyanling
 * @createTime 2020年08月03日 17:00.
 */
public class ZookeeperUtil {
    public static String responseTime_root = "/responseTime";
    //分割符号:
    public static String split_symbol = ":";
    public static String serverPathRoot = "/serverPath";
    //单个服务器节点
    public static String serverPath = serverPathRoot + "/server";

    public static CuratorFramework createSession() {

        RetryPolicy exponentialBackoffRetry = new ExponentialBackoffRetry(1000, 3);

        // 使用fluent编程风格
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2182")
//                .connectString("192.168.0.128:2181")
                .sessionTimeoutMs(50000)
                .connectionTimeoutMs(30000)
                .retryPolicy(exponentialBackoffRetry)
//                .namespace("base")  // 独立的命名空间 /base
                .build();
        client.start();

        createNode(client, serverPathRoot);
        createNode(client, responseTime_root);
        return client;
    }

    // 创建持久节点
    public static String createNode(CuratorFramework client, String path, String data) {
        String result = null;

        try {
            Stat stat = client.checkExists().forPath(path);
            if (stat == null) {//不存在，才创建
                result = client.create().creatingParentsIfNeeded().forPath(path, data.getBytes());
            } else {
                //存在就更新节点内容
                client.setData().withVersion(stat.getVersion()).forPath(path, data.getBytes()).getVersion();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    // 创建持久节点
    public static String createNode(CuratorFramework client, String path) {
        String result = null;

        try {
            Stat stat = client.checkExists().forPath(path);
            if (stat == null) {//不存在，才创建
                result = client.create().creatingParentsIfNeeded().forPath(path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 创建执行类型的节点
    public static String createNode(CuratorFramework client, CreateMode nodeType,
                                    String path, String data) {
        String result = null;
        try {
            result = client.create().creatingParentsIfNeeded()
                    .withMode(nodeType).forPath(path, data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 获取节点数据
    public static String getNodeData(CuratorFramework client, String path) {
        String result = null;

        try {
            Stat stat = client.checkExists().forPath(path);
            if (stat != null) {//存在，才查询
                byte[] bytes = client.getData().forPath(path);
                result = new String(bytes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 获取子节点
    public static List<String> getChildrens(CuratorFramework client, String path) {
        List<String> list = null;

        try {
            list = client.getChildren().forPath(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
