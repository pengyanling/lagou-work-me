package com.lagou.handler;

import com.lagou.pojo.RpcRequest;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.net.InetSocketAddress;
import java.util.concurrent.Callable;

/**
 * 自定义事件处理器
 */
public class UserClientHandler extends ChannelInboundHandlerAdapter implements Callable {

    //1.定义成员变量
    private ChannelHandlerContext context; //事件处理器上下文对象 (存储handler信息,写操作)
    private String result; // 记录服务器返回的数据
    private RpcRequest param; //记录将要发送给服务器的数据

    public boolean channelInactive =false; //通道不活跃

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("通道不活跃");
        channelInactive =true;
    }

    //2.实现channelActive  客户端和服务器连接时,该方法就自动执行
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //初始化ChannelHandlerContext
        this.context = ctx;
        InetSocketAddress ipSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        int port = ipSocket.getPort();
//        System.out.println("服务端建立连接port==========" + port);

        ctx.fireChannelActive();
    }

    //3.实现channelRead 当我们读到服务器数据,该方法自动执行
    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //将读到的服务器的数据msg ,设置为成员变量的值
        result = msg.toString();
        notify();
    }

    //4.将客户端的数写到服务器
    public synchronized Object call() throws Exception {
        //context给服务器写数据
        if (context != null) {
            if(!channelInactive){
                Channel channel = context.channel();
                channel.writeAndFlush(param);
            }
        } else {
            System.out.println("ChannelHandlerContext为null");

        }
        wait();
        return result;
    }

    //5.设置参数的方法
    public void setParam(RpcRequest param) {
        this.param = param;
    }
}
