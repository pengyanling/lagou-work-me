package com.lagou.boot;

import com.lagou.client.RPCConsumer;
import com.lagou.handler.UserClientHandler;
import com.lagou.pojo.RpcRequest;
import com.lagou.service.IUserService;
import com.lagou.zookeeper.ZookeeperUtil;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;

import java.util.List;

public class ConsumerBoot {

    //参数定义
    private static final String PROVIDER_NAME = "UserService#sayHello#";
    private static RpcRequest rpcRequest = new RpcRequest("com.lagou.service.UserServiceImpl");

    public static void main(String[] args) throws InterruptedException {

        //连接Zookeeper
        CuratorFramework client = ZookeeperUtil.createSession();

        createNewProxy(client);
        watcherNode(client, ZookeeperUtil.serverPathRoot);


        //2.循环给服务器写数据
        int i = 1;
        while (i < 1000) {
            String port = getPort(client);
            IUserService service = RPCConsumer.proxyMap.get(port);
            UserClientHandler handler = RPCConsumer.handlerMap.get(port);

            if (handler != null) {
                if (!handler.channelInactive) {
                    i = callServiceFun(i, service);
                } else {
                    RPCConsumer.proxyMap.remove(port);
                    RPCConsumer.handlerMap.remove(port);
                }
            } else {
                i = callServiceFun(i, service);
            }
        }
    }

    /**
     * 调用service的方法
     *
     * @param i
     * @param service
     * @return
     */
    private static int callServiceFun(int i, IUserService service) throws InterruptedException {
        String result = service.sayHello(" I am Client !!! " + i++);
        System.out.println(result);
        Thread.sleep(2000);
        return i;
    }

    private static String getPort(CuratorFramework client) {
        String port = null;
        List<String> responseTimeNodeList = ZookeeperUtil.getChildrens(client, ZookeeperUtil.responseTime_root);
        long maxTime = 0;//最新的时间
        for (String path : responseTimeNodeList) {
            String nodeData = ZookeeperUtil.getNodeData(client, ZookeeperUtil.responseTime_root + "/" + path);
            if (nodeData == null || "".equals(nodeData)) {
                continue;
            }
            long aLong = Long.valueOf(nodeData);
            if (maxTime == 0) {
                port = path;
                maxTime = aLong;
            } else if (maxTime < aLong) {
                port = path;
                maxTime = aLong;
            }
        }
        if (port == null || "".equals(port)) {
            for (String portF : RPCConsumer.proxyMap.keySet()) {
                port = portF;
                break;
            }
        } else {
            IUserService service = RPCConsumer.proxyMap.get(port);
            if (service == null) {
                for (String portF : RPCConsumer.proxyMap.keySet()) {
                    port = portF;
                    break;
                }
            }

        }
        return port;
    }

    /**
     * 创建新的代理对象
     *
     * @param client
     */
    private static void createNewProxy(CuratorFramework client) {
        RPCConsumer.proxyMap.clear();
        List<String> childrenList = ZookeeperUtil.getChildrens(client, ZookeeperUtil.serverPathRoot);
        if (childrenList != null && !childrenList.isEmpty()) {
            for (String path : childrenList) {
                String nodeData = ZookeeperUtil.getNodeData(client, ZookeeperUtil.serverPathRoot + "/" + path);
                if (nodeData == null || "".equals(nodeData)) {
                    continue;
                }
                String[] split = nodeData.split(ZookeeperUtil.split_symbol);
                String host = split[0];
                Integer port = Integer.valueOf(split[1]);
                //1.创建代理对象
                RPCConsumer.createProxy(IUserService.class, rpcRequest, host, port);
            }
        }
    }

    // 监听根节点
    public static void watcherNode(CuratorFramework client, String path) {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(client, path, true);
        try {
            pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
            pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {

                @Override
                public void childEvent(CuratorFramework client,
                                       PathChildrenCacheEvent event) throws Exception {
                    switch (event.getType()) {
                        case CHILD_ADDED:
                            System.out.println("添加了子节点  " + event.getData());
                            createNewProxy(client);
                            break;
                        case CHILD_UPDATED:
                            System.out.println("子节点发生了更新  " + event.getData());
                            createNewProxy(client);
                            break;
                        case CHILD_REMOVED:
                            System.out.println("子节点移除  " + event.getData());
                            createNewProxy(client);
                        case CONNECTION_SUSPENDED:
                            break;
                        // ZK挂掉
                        case CONNECTION_RECONNECTED:
                            break;
                        // 重新启动ZK
                        case CONNECTION_LOST:
                            break;
                        // ZK挂掉一段时间后
                        case INITIALIZED:
                            break;
                        default:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
