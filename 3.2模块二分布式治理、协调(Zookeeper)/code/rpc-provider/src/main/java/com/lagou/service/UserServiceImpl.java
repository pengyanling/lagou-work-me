package com.lagou.service;

import com.lagou.codec.RpcDecoder;
import com.lagou.handler.UserServiceHandler;
import com.lagou.pojo.RpcRequest;
import com.lagou.serial.JSONSerializer;
import com.lagou.zookeeper.ZookeeperUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements IUserService {
    String ip = "127.0.0.1";
    public static int port = 8999;

    //将来客户单要远程调用的方法
    public String sayHello(String msg) {
        System.out.println("are you ok ? " + msg);
        return "success" + port;
    }


    //创建一个方法启动服务器
    @PostConstruct
    public void startServer() throws InterruptedException {
        //1.创建两个线程池对象
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workGroup = new NioEventLoopGroup();

        //2.创建服务端的启动引导对象
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        CuratorFramework client = registerZookeeper();

        //3.配置启动引导对象
        serverBootstrap.group(bossGroup, workGroup)
                //设置通道为NIO
                .channel(NioServerSocketChannel.class)
                //创建监听channel
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //获取管道对象
                        ChannelPipeline pipeline = nioSocketChannel.pipeline();
                        //给管道对象pipeLine 设置编码

                        pipeline.addLast(new StringEncoder());
//                        pipeline.addLast(new RpcEncoder(RpcRequest.class,new JSONSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                        //把我们自顶一个ChannelHander添加到通道中
                        pipeline.addLast(new UserServiceHandler(client,port));
                    }
                });

        //4.绑定端口
        ChannelFuture channelFuture = serverBootstrap.bind(ip, port).sync();
        channelFuture.addListener((ChannelFuture futureListener) -> {
            final EventLoop eventLoop = futureListener.channel().eventLoop();
            //每秒检查：如果5秒内都没有新的响应，就清除当前服务在zookeeper对应的响应节点
            eventLoop.schedule(() -> doConnect(client,channelFuture), 1, TimeUnit.SECONDS);
        });
    }

    /**
     * 重新连接tcp服务端
     */
    public static void doConnect(CuratorFramework client,ChannelFuture channelFuture) {
        try {
            long end = System.currentTimeMillis();
            String path = ZookeeperUtil.responseTime_root + "/" + port;
            String nodeData = ZookeeperUtil.getNodeData(client, path);
            if (nodeData != null) {
                Long responseTime = Long.valueOf(nodeData);
                if (end - responseTime >= 5000) {
                    client.delete().forPath(path);
                    System.out.println("删除响应时间节点!" + path);
                }
            }
        } catch (Exception e) {
            System.out.println("客户端连接失败!" + e.getMessage());
        }
        channelFuture.addListener((ChannelFuture futureListener) -> {
            final EventLoop eventLoop = futureListener.channel().eventLoop();
            //每秒检查：如果5秒内都没有新的响应，就清除当前服务在zookeeper对应的响应节点
            eventLoop.schedule(() -> doConnect(client,channelFuture), 1, TimeUnit.SECONDS);
        });
    }

    /**
     * 注册到zookeeper中
     */
    public CuratorFramework registerZookeeper() {
        CuratorFramework client = ZookeeperUtil.createSession();
        String data = ip + ZookeeperUtil.split_symbol + port;
        ZookeeperUtil.createNode(client, CreateMode.EPHEMERAL_SEQUENTIAL, ZookeeperUtil.serverPath, data);
        return client;
    }
}
