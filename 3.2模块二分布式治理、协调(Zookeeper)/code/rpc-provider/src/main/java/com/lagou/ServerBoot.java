package com.lagou;

import com.lagou.service.UserServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerBoot {


    public static void main(String[] args) throws InterruptedException {
        UserServiceImpl.port=Integer.valueOf(args[0]);
        System.out.println(UserServiceImpl.port);
        //启动服务器
        SpringApplication.run(ServerBoot.class, args);
//        UserServiceImpl.startServer("127.0.0.1", 8999);
    }
}
