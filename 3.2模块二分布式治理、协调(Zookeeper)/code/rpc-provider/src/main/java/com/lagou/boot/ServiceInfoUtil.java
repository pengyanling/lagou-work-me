package com.lagou.boot;

import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

/**
 * @author pengyanling
 * @createTime 2020年08月03日 19:19.
 */
@Configuration
public class ServiceInfoUtil {
    @EventListener(WebServerInitializedEvent.class)
    public void onWebServerReady(WebServerInitializedEvent event) {
//        System.out.println("1.当前WebServer实现类为："+event.getWebServer().getClass().getName());
        WebServerApplicationContext applicationContext = event.getApplicationContext();
         int port = applicationContext.getWebServer().getPort();
        System.out.println("当前Web端口："+port);
//        String name = applicationContext.getWebServer().getClass().getName();
    }
}
