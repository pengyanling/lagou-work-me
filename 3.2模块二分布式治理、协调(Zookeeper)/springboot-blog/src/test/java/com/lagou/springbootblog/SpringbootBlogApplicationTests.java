package com.lagou.springbootblog;

import com.lagou.mapper.ArticleMapper;
import com.lagou.pojo.Article;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringbootBlogApplicationTests {


    @Autowired
    private ArticleMapper articleMapper;


    @Test
    public void findAllArticle(){
        List<Article> all = articleMapper.findAll();
        for (Article article : all) {
            System.out.println(article);

        }
    }
}
