import com.lagou.zookeeper.ZookeeperUtil;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.data.Stat;

/**
 * @author pengyanling
 * @createTime 2020年08月05日 21:13.
 */
public class TestMain {
    public static void main(String[] args) throws Exception {
        CuratorFramework client = ZookeeperUtil.createSession();
//        ZookeeperUtil.createNode(client,ZookeeperUtil.configPathRoot + "/url",
//                "jdbc:mysql://localhost:3306/blog_system?serverTimezone=UTC");
//        ZookeeperUtil.createNode(client,ZookeeperUtil.configPathRoot + "/username","root");
//        ZookeeperUtil.createNode(client,ZookeeperUtil.configPathRoot + "/password","root");
        String urlPath = ZookeeperUtil.configPathRoot + "/url";
        String url = "jdbc:mysql://localhost:3306/jpa?serverTimezone=UTC";
//        String url = "jdbc:mysql://localhost:3306/blog_system?serverTimezone=UTC";
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath(urlPath);

        client.setData().withVersion(stat.getVersion()).forPath(urlPath, url.getBytes()).getVersion();
        byte[] bytes1 = client.getData().forPath(urlPath);
        System.out.println(new String(bytes1));
    }
}
