package com.lagou.controller;

import com.lagou.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ArticleController {
    @Autowired
    private IArticleService articleService;

    @RequestMapping("/toIndexPage")
    public String toLoginPage( Model model,
                               @RequestParam(required = false,
                                       defaultValue="1",value="pageNum")Integer pageNum,
                               @RequestParam(defaultValue="2",value="pageSize")Integer pageSize){
        if(pageNum==null || pageNum<=0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 2;
        }
        articleService.findAll(model,pageNum,pageSize);
        return "client/index";
    }


}
