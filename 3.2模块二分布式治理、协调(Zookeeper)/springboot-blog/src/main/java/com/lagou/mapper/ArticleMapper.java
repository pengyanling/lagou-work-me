package com.lagou.mapper;

import com.lagou.pojo.Article;

import java.util.List;


public interface ArticleMapper {
    List<Article> findAll();

}
