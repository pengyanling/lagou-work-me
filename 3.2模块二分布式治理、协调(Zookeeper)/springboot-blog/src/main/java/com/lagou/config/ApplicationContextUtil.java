package com.lagou.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * @author pengyanling
 * @createTime 2020年07月28日 9:11.
 */
@Component
public class ApplicationContextUtil implements ApplicationContextAware {
    private static AnnotationConfigWebApplicationContext annWeb;//声明一个静态变量保存
    public static AnnotationConfigServletWebServerApplicationContext annServletWeb;//声明一个静态变量保存

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("applicationContext正在初始化：" + applicationContext);
        AnnotationConfigServletWebServerApplicationContext aab;
        if (applicationContext instanceof AnnotationConfigWebApplicationContext)
            this.annWeb = (AnnotationConfigWebApplicationContext) applicationContext;
        if (applicationContext instanceof AnnotationConfigServletWebServerApplicationContext)
            this.annServletWeb = (AnnotationConfigServletWebServerApplicationContext) applicationContext;
    }

    public static <T> T getBean(Class<T> clazz) {
        if (annServletWeb == null) {
            System.out.println("applicationContext是空的");
        } else {
            System.out.println("applicationContext不是空的");
        }
        return annServletWeb.getBean(clazz);
    }

    public static void registerDataSource() {
        String beanName = "druidDataSource";
        if (annServletWeb.containsBean(beanName)) {
            annServletWeb.removeBeanDefinition(beanName);
        }
       /* if (annServletWeb.containsBean(beanName)) {
            annServletWeb.removeBeanDefinition(beanName);
        }
        //默认使用对象的无参构造方法：此对象已经重写无参构造（此处会重新实例化一个新对象）
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(DruidDataSource.class);
        //构造注入对象参数
        AbstractBeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
        beanDefinition.setAutowireMode(2);
        beanDefinition.setPrimary(true);
        beanDefinition.setSynthetic(true);
        beanDefinition.setScope("singleton");
        beanDefinition.setRole(0);

        annServletWeb.registerBeanDefinition(beanName, beanDefinition);*/
        ConfigurableListableBeanFactory beanFactory = annServletWeb.getBeanFactory();
        beanFactory.registerSingleton(beanName, new DruidDataSource());

    }

}
