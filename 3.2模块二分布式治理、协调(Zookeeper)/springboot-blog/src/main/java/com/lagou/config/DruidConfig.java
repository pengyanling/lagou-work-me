package com.lagou.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.lagou.zookeeper.ZookeeperUtil;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author pengyanling
 * @createTime 2020年08月05日 9:22.
 */
@Configuration
public class DruidConfig {
    public static CuratorFramework client = ZookeeperUtil.createSession();

    @Bean
    public DataSource druidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        setDataSourceField(druidDataSource);
        watcherNode(ZookeeperUtil.configPathRoot);
        return druidDataSource;
    }

    private static void setDataSourceField(DruidDataSource druidDataSource) {
        String url = ZookeeperUtil.getNodeData(client, ZookeeperUtil.configPathRoot + "/url");
        String username = ZookeeperUtil.getNodeData(client, ZookeeperUtil.configPathRoot + "/username");
        String password = ZookeeperUtil.getNodeData(client, ZookeeperUtil.configPathRoot + "/password");
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
    }

    // 监听根节点
    public static void watcherNode(String path) {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(client, path, true);
        try {
            pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
            pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {

                @Override
                public void childEvent(CuratorFramework client,
                                       PathChildrenCacheEvent event) throws Exception {
                    switch (event.getType()) {
                        case CHILD_ADDED:
                            System.out.println("添加了子节点  " + event.getData());
                            break;
                        case CHILD_UPDATED:
                            System.out.println("子节点发生了更新  " + event.getData());
                            ApplicationContextUtil.registerDataSource();
                            DruidDataSource druidDataSource = ApplicationContextUtil.getBean(DruidDataSource.class);
                            setDataSourceField(druidDataSource);
                            break;
                        case CHILD_REMOVED:
                            System.out.println("子节点移除  " + event.getData());
                        case CONNECTION_SUSPENDED:
                            break;
                        // ZK挂掉
                        case CONNECTION_RECONNECTED:
                            break;
                        // 重新启动ZK
                        case CONNECTION_LOST:
                            break;
                        // ZK挂掉一段时间后
                        case INITIALIZED:
                            break;
                        default:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
