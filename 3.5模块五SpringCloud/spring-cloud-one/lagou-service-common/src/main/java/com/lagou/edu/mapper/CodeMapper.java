package com.lagou.edu.mapper;

import com.lagou.edu.pojo.AuthCode;
import org.apache.ibatis.annotations.Param;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:20.
 */
public interface CodeMapper {
    AuthCode findCode(@Param("email") String email);

    int addCode(AuthCode codeBean);

    int modCode(AuthCode codeBean);
}
