package com.lagou.edu.util;

import com.lagou.edu.pojo.AuthCode;

import java.util.Date;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 22:44.
 */
public class CodeUtils {
    /**
     * 校验验证码【0-正确，1-错误，2-超时】
     * @param dbCode
     * @param code
     * @return
     */
    public static int validateCode(AuthCode dbCode,String code) {
        int result = 1;//错误
        if (dbCode != null) {
            if (dbCode.getCode().equals(code)) {
                result = 0;//正确
                if (dbCode.getExpiretime().compareTo(new Date()) <= 0) {//验证码已过期
                    result = 2;//超时
                }
            }
        }
        return result;
    }
}
