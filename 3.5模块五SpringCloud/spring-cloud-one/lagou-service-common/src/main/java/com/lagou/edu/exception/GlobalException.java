package com.lagou.edu.exception;

/**
 * 系统全局异常基类，所有异常都应继承此类
 */
public class GlobalException extends RuntimeException {

    private static final long serialVersionUID = 8296936943004836528L;

    public GlobalException() {
        super();
    }

    public GlobalException(String message) {
        super(message);
    }

    public GlobalException(Throwable cause) {
        super(cause);
    }

    public GlobalException(String message, Throwable cause) {
        super(message, cause);
    }
}
