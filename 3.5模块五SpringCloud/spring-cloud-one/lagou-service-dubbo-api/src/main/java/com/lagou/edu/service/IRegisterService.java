package com.lagou.edu.service;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 22:21.
 */
public interface IRegisterService {
    boolean isRegistered(String email);
}
