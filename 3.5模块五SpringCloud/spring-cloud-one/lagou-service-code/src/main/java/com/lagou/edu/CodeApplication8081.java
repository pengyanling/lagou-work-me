package com.lagou.edu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author pengyanling
 * @createTime 2020年08月26日 15:59.
 */
@MapperScan(basePackages = {"com.lagou.edu.mapper"}) //如果不配置，就找不到父module中的mapper
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class CodeApplication8081 {
    public static void main(String[] args) {
        SpringApplication.run(CodeApplication8081.class,args);
    }
}
