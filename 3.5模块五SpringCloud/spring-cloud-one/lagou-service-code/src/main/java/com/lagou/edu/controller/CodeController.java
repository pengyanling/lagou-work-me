package com.lagou.edu.controller;

import com.lagou.edu.service.ICodeService;
import com.lagou.edu.service.IRegisterService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 9:08.
 */
@RestController
@RequestMapping("/code")
public class CodeController {
    @Autowired
    private ICodeService codeService;
    @Reference
    private IRegisterService registerService;

    @PostMapping("/create/{email}")
    public boolean createCode(@PathVariable String email) {
        return codeService.createCode(email);
    }

    @GetMapping("/validate/{email}/{code}")
    public int validateCode(@PathVariable String email, @PathVariable String code) {
        return codeService.validateCode(email, code);
    }

    @GetMapping("/isRegistered/{email}")
    public boolean isRegistered(@PathVariable String email) {
        return registerService.isRegistered(email);
    }
}
