import java.util.Date;

/**
 * @author pengyanling
 * @createTime 2020年08月29日 10:15.
 */
public class TestMain {
    public static void main(String[] args) {
        Date date = new Date();
        Date date1 = new Date(date.getTime() + 1000 * 60 * 10);
        System.out.println(date);
        System.out.println(date1);
    }
}
