import com.lagou.edu.CodeApplication8081;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

/**
 * @author pengyanling
 * @createTime 2020年09月05日 16:20.
 */
@SpringBootTest(classes = {CodeApplication8081.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class CodeTest {
    @Autowired
    private RestTemplate restTemplate;
    @Test
    public void testRibbon() {
        String url = "http://lagou-service-user/user/isRegistered/574069214@qq.com";
        Integer forObject = restTemplate.getForObject(url, Integer.class);
        System.out.println("=====》》》使用ribbon负载均衡访问，访问的服务实例的端口号：" + forObject);
    }
}
