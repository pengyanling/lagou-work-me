package com.lagou.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author pengyanling
 * @createTime 2020年08月28日 10:53.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GateWayApplication9002 {
    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication9002.class, args);
    }
}
