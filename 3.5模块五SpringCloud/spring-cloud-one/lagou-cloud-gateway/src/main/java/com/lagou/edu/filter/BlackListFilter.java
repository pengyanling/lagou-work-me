package com.lagou.edu.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 定义全局过滤器，会对所有路由生效
 */
@Slf4j
@Component  // 让容器扫描到，等同于注册了
@RefreshScope
public class BlackListFilter implements GlobalFilter, Ordered {
    @Value("${ip-refresh-control.minute}")
    private int minute;//分钟数
    @Value("${ip-refresh-control.refresh.times}")
    private int refreshTimes;//次数
    // 默认限制时间（单位：ms）
    private static final long LIMITED_TIME_MILLIS = 30 * 1000;
    //ip访问次数、时间的记录
    static ConcurrentHashMap<String, LinkedList<Long>> ipMap = new ConcurrentHashMap<String, LinkedList<Long>>();

    static ConcurrentHashMap<String, Long> blackIpMap = new ConcurrentHashMap<String, Long>();//被限制的ip

    static {

        // 每30秒检查队列，移除超过限制时间的ip
        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(new Runnable() {
                    @Override
                    public void run() {
                        if (!blackIpMap.isEmpty()) {
                            for (String ip : blackIpMap.keySet()) {
                                if (blackIpMap.get(ip) <= System.currentTimeMillis()) {
                                    log.info("=============" + ip + " IP从黑名单中移除。");
                                    blackIpMap.remove(ip);
                                }
                            }
                        }
                    }
                }, 5, 30, TimeUnit.SECONDS);
    }

    /**
     * 过滤器核心方法
     *
     * @param exchange 封装了request和response对象的上下文
     * @param chain    网关过滤器链（包含全局过滤器和单路由过滤器）
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 从上下文中取出request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //跨域设置
        HttpHeaders headers = response.getHeaders();
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "*");

        // 从request对象中获取客户端ip
        String clientIp = request.getRemoteAddress().getHostString();
        Mono<Void> wrap1 = checkIpBlack(response, clientIp);
        if (wrap1 != null) return wrap1;

        URI uri = request.getURI();
        String host = uri.getHost();
        String path = uri.getPath();
        if (path.contains("/api/email/")) {//发送邮件，必须要有token
            MultiValueMap<String, HttpCookie> cookies = request.getCookies();
            List<HttpCookie> token = cookies.get("token");
            if (token == null || "".equals(token)) {
                DataBuffer wrap = getDataBuffer(response, clientIp + " token不合法。", "token不合法，请求已被拒绝!");
                return response.writeWith(Mono.just(wrap));
            }
        }
        // 合法请求，放行，执行后续的过滤器
        return chain.filter(exchange);
    }

    /**
     * 校验IP是否在黑名单
     *
     * @param response
     * @param clientIp
     * @return
     */
    private Mono<Void> checkIpBlack(ServerHttpResponse response, String clientIp) {
        if (blackIpMap.containsKey(clientIp)) {
            DataBuffer wrap = getDataBuffer(response, clientIp + " IP在黑名单中。", "您所在的IP地址访问过于频繁，请稍后再试。");
            return response.writeWith(Mono.just(wrap));
        } else {
            LinkedList<Long> timesList = ipMap.get(clientIp);
            long currentTime = System.currentTimeMillis();
            if (timesList == null) {
                timesList = new LinkedList<>();
                ipMap.put(clientIp, timesList);
            } else if(!timesList.isEmpty()) {

                Long first = timesList.getFirst();
                while (first + minute * 60 * 1000 < currentTime) {//minute的单位是：分钟
                    timesList.removeFirst();
                    first = timesList.getFirst();
                    if (first == null) {
                        break;
                    }
                }
            }
            if (timesList.size() > refreshTimes) {
                ipMap.remove(clientIp);
                blackIpMap.put(clientIp, System.currentTimeMillis() + LIMITED_TIME_MILLIS);
                DataBuffer wrap = getDataBuffer(response, clientIp + " IP加入黑名单中。", "您所在的IP地址访问过于频繁，请稍后再试。");
                return response.writeWith(Mono.just(wrap));
            } else {
                timesList.addLast(currentTime);
            }
        }
        return null;
    }

    private DataBuffer getDataBuffer(ServerHttpResponse response, String logInfo, String returnMessage) {
        // 拒绝访问，返回
        response.setStatusCode(HttpStatus.SEE_OTHER); // 状态码
        log.info(logInfo);
        return response.bufferFactory().wrap(new String(returnMessage.getBytes(), StandardCharsets.UTF_8).getBytes());
    }


    /**
     * 返回值表示当前过滤器的顺序(优先级)，数值越小，优先级越高
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
