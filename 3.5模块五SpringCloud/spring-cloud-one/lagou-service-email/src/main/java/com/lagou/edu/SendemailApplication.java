package com.lagou.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 14:20.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SendemailApplication {
    public static void main(String[] args) {
        SpringApplication.run(SendemailApplication.class,args);
    }
}
