import com.lagou.edu.SendemailApplication;
import com.lagou.edu.email.IMailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 11:44.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SendemailApplication.class})
public class SendemailApplicationTests {
    /**
     * 注入发送邮件的接口
     */
    @Autowired
    private IMailService mailService;

    /**
     * 测试发送文本邮件
     */
    @Test
    public void sendmail() {
        mailService.sendSimpleMail("574069214@qq.com","SpringCloud","234145");
    }

    @Test
    public void sendmailHtml(){
        //TODO 这个发送失败
        //SMTPAddressFailedException: 550 Invalid User: ;`}html®ö
        mailService.sendHtmlMail("1357930737@qq.com","主题：你好html邮件","<h1>内容：第一封html邮件</h1>");
    }
}
