package com.lagou.edu.service.impl;

import com.lagou.edu.mapper.CodeMapper;
import com.lagou.edu.mapper.TokenMapper;
import com.lagou.edu.pojo.LagouToken;
import com.lagou.edu.service.IUserService;
import com.lagou.edu.util.ApiAssert;
import com.lagou.edu.util.CodeUtils;
import com.lagou.edu.util.MyErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 22:21.
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private CodeMapper codeMapper;
    @Autowired
    private TokenMapper tokenMapper;

    @Override
    public boolean register(String email, String password, String code,
                            HttpServletResponse response) {
        int valicate = CodeUtils.validateCode(codeMapper.findCode(email), code);
        //校验验证码：是否正确，是否超时，若有问题，准确提示给用户
        ApiAssert.isFalse(MyErrorCode.getErrorCode400(true, "验证码错误。"), valicate == 1);
        ApiAssert.isFalse(MyErrorCode.getErrorCode400(true, "验证码超时。"), valicate == 2);

        LagouToken tokenBean = new LagouToken();
        tokenBean.setEmail(email);
        tokenBean.setToken(UUID.randomUUID().toString());
        tokenMapper.addToken(tokenBean);
        //将token存入cookie
        response.setHeader("Access-Control-Expose-Headers","tokenCookie");
        response.addHeader("tokenCookie",tokenBean.getToken());
        response.addCookie(new Cookie("token", tokenBean.getToken()));
        return valicate == 0 ? true : false;
    }

    @Override
    public String login(String email, String password,
                        HttpServletResponse response) {
        //登录接口，验证用户名密码合法性，根据用户名和密码生成token，
        // token存入数据库，并写入cookie中，
        //登录成功返回邮箱地址，重定向到欢迎页
        LagouToken tokenBean = new LagouToken();
        tokenBean.setEmail(email);
        tokenBean.setToken(UUID.randomUUID().toString());
        tokenMapper.modToken(tokenBean);
        //将token存入cookie
        response.setHeader("Access-Control-Expose-Headers","tokenCookie");
        response.addHeader("tokenCookie",tokenBean.getToken());
        response.addCookie(new Cookie("token", tokenBean.getToken()));
        return email;
    }

    @Override
    public String info(String token) {
        // 根据token查询用户登录邮箱接口
        return tokenMapper.findByToken(token);
    }
}
