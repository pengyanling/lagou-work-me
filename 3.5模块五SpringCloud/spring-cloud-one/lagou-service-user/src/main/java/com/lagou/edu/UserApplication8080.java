package com.lagou.edu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 14:20.
 */
@MapperScan(basePackages = {"com.lagou.edu.mapper"}) //如果不配置，就找不到父module中的mapper
@SpringBootApplication
@EnableDiscoveryClient
public class UserApplication8080 {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication8080.class,args);
    }
}
