package com.lagou.edu.service.impl;

import com.lagou.edu.mapper.TokenMapper;
import com.lagou.edu.pojo.LagouToken;
import com.lagou.edu.service.IRegisterService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author pengyanling
 * @createTime 2020年09月06日 11:59.
 */
@Service
public class RegisterServiceImpl implements IRegisterService {
    @Autowired
    private TokenMapper tokenMapper;
    @Override
    public boolean isRegistered(String email) {
        //是否已注册，根据邮箱判断,true代表已经注册过，false代表尚未注册
        LagouToken token = tokenMapper.findByEmail(email);
        return token != null ? true : false;
    }
}
