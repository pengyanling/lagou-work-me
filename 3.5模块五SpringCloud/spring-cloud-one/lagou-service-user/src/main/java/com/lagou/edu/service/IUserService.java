package com.lagou.edu.service;

import javax.servlet.http.HttpServletResponse;

/**
 * @author pengyanling
 * @createTime 2020年08月27日 22:21.
 */
public interface IUserService {
    boolean register(String email, String password, String code,
                     HttpServletResponse response);
    String login(String email, String password,
                 HttpServletResponse response);
    String info(String token);
}
