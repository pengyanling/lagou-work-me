package com.lagou.service;

import org.springframework.ui.Model;

/**
 * @author pengyanling
 * @createTime 2020年07月13日 15:30.
 */
public interface IArticleService {
    void findAll(Model model, int pageNum, int pageSize);
}
