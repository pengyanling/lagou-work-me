package com.lagou.edu.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lagou.edu.config.IConstant.*;

/**
 * @author pengyanling
 * @createTime 2020年10月26日 11:34.
 */
@RestController
public class GoController {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    @RequestMapping("/go")
    public String distributeGo() {
        rabbitTemplate.convertAndSend(change_name, routing_Key, "送单到石景山x小区，请在15秒内接受任务");
        return "任务已经下发，等待送单。。。";
    }

    @RequestMapping("/notgo")
    public Long getAccumulatedTask() {
        Long id = (Long) rabbitTemplate.receiveAndConvert(queue_name_dlx);
        return id;
    }
}
