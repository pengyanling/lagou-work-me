package com.lagou.edu.controller;

import com.lagou.edu.mapper.OrderMapper;
import com.lagou.edu.pojo.OrderBean;
import com.lagou.edu.pojo.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

import static com.lagou.edu.config.IConstant.*;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Autowired
    private OrderMapper orderMapper;

    @RequestMapping("/orders")
    @ResponseBody
    public List<OrderBean> orderList(HttpServletRequest request, HttpServletResponse response) {
        return orderMapper.findAll();
    }

    @PostMapping(value = "/add", consumes = "application/x-www-form-urlencoded")
    public ModelAndView insert(@ModelAttribute OrderBean bean) {
        ModelAndView mov = new ModelAndView("pay");
        Map<String, Object> model = mov.getModel();
        model.put("id", bean.getId());
        orderMapper.save(bean);
        rabbitTemplate.convertAndSend(change_name, routing_Key, (bean.getId()+"").getBytes());//入消息队列
        return mov;
    }

    //支付
    @GetMapping("/payMoney")
    public String payMoney(@Param("id") Long paramId) {
        byte[] idByte = (byte[]) rabbitTemplate.receiveAndConvert(queue_name);

        orderMapper.payMoney(Long.parseLong(new String(idByte)));
        return "success";
    }

    //取消订单
    @PostMapping("/cancelOrder")
    public @ResponseBody
    Result cancelOrder() {
        Long id = (Long) rabbitTemplate.receiveAndConvert(queue_name_dlx);
        orderMapper.cancelOrder(id);
        return new Result("cancel Order success");
    }

    /*@PostMapping("/cancelOrder")
    public @ResponseBody
    Result cancelOrder(HttpServletRequest req) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String s = null;
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            JSONObject jsonObject = JSONObject.parseObject(sb.toString());
            orderMapper.cancelOrder(jsonObject.getLong("id"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Result("cancel Order success");
    }*/
//
//    @GetMapping("/del/{id}")
//    public @ResponseBody Result delete(@PathVariable("id") Long id) {
//        orderMapper.deleteById(id);
//        return new Result("delete success");
//    }


    @PostMapping("/mod")
    public @ResponseBody
    Result mod(@RequestBody OrderBean bean) {
        orderMapper.mod(bean);
        return new Result("modify success");
    }

    @RequestMapping("/toLogin")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        return "success";
    }
}
