<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>查看订单列表</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <style type="text/css">
        table{
            border: 1px solid #d9cece;
        }
        th,td{
            width: 100px;
            border: 1px solid #d9cece;
            text-align: center;
        }
        .saveBtn{
            display: none;
        }
    </style>
</head>
<body>
<h2>我是服务器：${pageContext.request.localPort}</h2>
<h2>当前sessionId：${pageContext.session.id}</h2>
<div style="height: 95%">
    <div><a href="#" id="addBtn">新增订单</a></div>
    <form method="post" action="${pageContext.request.contextPath}/login/add" enctype="application/x-www-form-urlencoded">
        <table id="resumeTable">
        </table>
    </form>
</div>
<script>
    $(function () {
        initTable();

        $(".modBtn").on("click",function(){
            $(this).parents("tr").find("input[name !='id']").removeAttr("readonly");
            $(this).next(".saveBtn").show();
            $(this).hide();
        })
        $(".saveBtn").on("click",function(){
            var allInput = $(this).parents("tr").find("input");
            saveResume(allInput);
        });
        $(".delBtn").on("click",function(){
            var idInput = $(this).parents("tr").find("input[name ='id']");
            var id =$(idInput).val();
            delResume(id);
        });
        $("#addBtn").on("click",function(){
            var aa = "<tr> "+
                "<td><input name='id'  /></td>"+
                "<td><input name='name'   /></td>"+
                "<td><input name='price'  /></td>"+
                "<td><input name='num'  /></td>"+
                "<td></td>"+
                "<td><input type='submit' value='保存'></td>"+
                "</tr>";
            $("#resumeTable").append(aa);
        })
    });

    function addSaveResume(_this) {
        var allInput = $(_this).parents("tr").find("input");
        var  id =$(allInput[0]).val();
        var  name =$(allInput[1]).val();
        var  price =$(allInput[2]).val();
        var  num =$(allInput[3]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/login/add',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"id":id,"name":name,
                "price":price,"num":num}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function delResume(id) {
        $.ajax({
            url: '${pageContext.request.contextPath}/login/del/'+id,
            type: 'get',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function saveResume(allInput) {
        var  id =$(allInput[0]).val();
        var  name =$(allInput[1]).val();
        var  price =$(allInput[2]).val();
        var  num =$(allInput[3]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/login/mod',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"id":id,"name":name,
                "price":price,"num":num}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function initTable() {
        $.ajax({
            url: '${pageContext.request.contextPath}/login/orders',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                $("#resumeTable").empty();
                var aa="<tr><th>Id</th><th>商品名称</th><th>单价</th><th>数量</th><th>支付状态</th></tr>";
                for (var i=0;i<data.length;i++) {
                    aa += "<tr> "+
                        "<td>"+data[i].id+"</td>"+
                        "<td>"+data[i].name+"</td>"+
                        "<td>"+data[i].price+"</td>"+
                        "<td>"+data[i].num+"</td>"+
                        "<td>"+data[i].payState+"</td>"+
                        "<td></td>"+
                        "</tr>";
                }
                $("#resumeTable").append(aa);
            }
        })
    }
</script>
</body>
</html>