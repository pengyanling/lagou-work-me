<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>支付页面</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery.timers-1.2.js"></script>
    <style type="text/css">
        table{
            border: 1px solid #d9cece;
        }
        th,td{
            width: 150px;
            border: 1px solid #d9cece;
            text-align: center;
        }
        .payBtn{
            display: none;
        }
    </style>
</head>
<body>
<h2>我是服务器：${pageContext.request.localPort}</h2>
<h2>当前sessionId：${pageContext.session.id}</h2>
<form method="get" action="${pageContext.request.contextPath}/login/payMoney">
    <table>
        <tr>
            <td><span>支付订单id</span></td>
            <td>
                <input type="text" readonly id="id" name="id" value="${id}" />
            </td>
            <td>
                <input id="payBtn" type="submit" value="支付">
            </td>
            <td>
                剩余支付时间：<span id="timeText" style="color: red;font-weight: bolder;"></span>秒
            </td>
        </tr>
    </table>
</form>
<script>
    $(function () {
        //一分钟内只允许获取一次验证码
        //每秒调用一次倒计时方法
        $('body').everyTime('1s','A',countdown);
    });
var time = 15;
function countdown(){
    if (time == 0) {
        //这里时设置当时间到0的时候重新设置点击事件，并且默认time修改为15
        $("#payBtn").remove();
        $('body').stopTime('A');
        time = 15;
        // ajaxCancelOrder();//定时取消订单
        window.location.href="/index.jsp";
    }else{
        //这里是显示时间倒计时
        time--;
        $("#timeText").text(time);
    }
}
    //手动取消订单
    function ajaxCancelOrder(){
        $.ajax({
            url: '${pageContext.request.contextPath}/login/cancelOrder',
            type: 'POST',    //GET
            async: false,    //是否异步
            data: JSON.stringify({"id":$("#id").val()}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                window.location.href="/index.jsp";
            }
        })
    }
</script>
</body>
</html>