package com.lagou.edu;

import com.lagou.edu.model.OrderInfo;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ProducerApplication.class})
public class ProducerApplicationTests {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Test
    public void test1() {
        int id=1000001;
        System.out.println("生成的订单id:" + id);
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId(id);
        orderInfo.setCommodity(1);
        orderInfo.setStatus(0);
        rocketMQTemplate.convertAndSend("springboot-mq-2", orderInfo);
    }
}
