package com.lagou.edu.service;

import com.lagou.edu.mapper.OrderInfoMapper;
import com.lagou.edu.model.OrderInfo;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

import static com.lagou.edu.IConstant.key_num;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 13:59.
 */
@Service
public class OrderService {

    @Autowired
    private OrderInfoMapper orderInfoMapper;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;


    public Integer submitOrder(Integer commodity) {
        Integer id = getOrderNum();
        System.out.println("生成的订单id:" + id);
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId(id);
        orderInfo.setCommodity(commodity);
        orderInfo.setStatus(0);

        boolean flag = subtractStock(1);

        if (!flag) {
            return -1;
        }
        rocketMQTemplate.convertAndSend("springboot-mq-2", orderInfo);

        return id;
    }

    public Integer pay(Integer id) {
        Boolean delete = redisTemplate.delete("order:" + id);
        System.out.println(delete);
        if (delete) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOrderId(id);
            orderInfo.setStatus(1);
            orderInfoMapper.updateStatus(orderInfo);
            return 1;
        }
        return 0;
    }

    /**
     * 模拟订单号
     *
     * @return
     */
    public Integer getOrderNum() {
        Random random = new Random();
        return random.nextInt(10000);
    }

    /**
     * 减库存
     * 【Redis乐观锁实现秒杀】
     *
     * @param num
     * @return
     */
    public boolean subtractStock(Integer num) {
        String key = key_num;
        System.out.println(redisTemplate.opsForValue().get(key));
        redisTemplate.setEnableTransactionSupport(true);
        redisTemplate.watch(key);

        Object value = redisTemplate.opsForValue().get(key);
        if (value == null) {
            //前提 提前将商品库存放入缓存 ,如果缓存不存在，视为没有该商品
            return false;
        }
        //先检查 库存是否充足
        Integer stock = Integer.valueOf(value.toString());

        if (stock < num) {
            System.out.println("库存不足");
            return false;
        }

        redisTemplate.multi();
        Long newStock = redisTemplate.opsForValue().increment(key, -num.longValue());//库存减少num个
        System.out.println(newStock);
        List<Object> exec = redisTemplate.exec();

        if (exec != null && exec.size() > 0) {
            return true;
        }

        return false;
    }

    public List<OrderInfo> findAll() {
        return orderInfoMapper.findAll();
    }
}

