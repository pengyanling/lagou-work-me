package com.lagou.edu.task;

import com.lagou.edu.mapper.WarehouseMapper;
import com.lagou.edu.model.Warehouse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import static com.lagou.edu.IConstant.key_num;

/**
 * 项目启动时，执行一次【初始化redis】
 *
 * @author pengyanling
 * @createTime 2019年10月21日 17:25.
 */
@Slf4j
@Component
@AllArgsConstructor
public class InitRedisApplicationRunner implements ApplicationRunner {
    private final WarehouseMapper warehouseMapper;
    private final StringRedisTemplate redisTemplate;


    @Override
    public void run(ApplicationArguments var1) {
        try {
            Warehouse db = warehouseMapper.selectByPrimaryKey(1);
            log.info("“初始化redis库存”-------------开始");

            redisTemplate.opsForValue().set(key_num,db.getNum()+"");
            log.info("“初始化redis库存”-------------成功");
            Object num = redisTemplate.opsForValue().get(key_num);
            log.info(num.toString());
        } catch (Exception e) {
            log.info("“初始化redis库存”-------------失败", e);
        }

    }
}
