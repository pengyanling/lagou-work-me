package com.lagou.edu.controller;

import com.lagou.edu.model.OrderInfo;
import com.lagou.edu.service.OrderService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {


    @Autowired
    private OrderService orderService;

    @RequestMapping("/orders")
    @ResponseBody
    public List<OrderInfo> orderList(HttpServletRequest request, HttpServletResponse response) {
        return orderService.findAll();
    }

    @PostMapping(value = "/add")
    public ModelAndView insert(@Param("id") int commodity) {
        ModelAndView mov = new ModelAndView("pay");
        Map<String, Object> model = mov.getModel();
        model.put("orderId", orderService.submitOrder(commodity));
        return mov;
    }

    //支付
    @GetMapping("/payMoney")
    public String payMoney(@Param("orderId") int orderId) {
        switch (orderService.pay(orderId)) {
            case 1:
                return "success";
            default:
                return "fail";
        }
    }


    @RequestMapping("/toLogin")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        return "success";
    }
}
