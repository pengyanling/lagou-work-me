<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>支付页面</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <style type="text/css">
        table{
            border: 1px solid #d9cece;
        }
        th,td{
            width: 100px;
            border: 1px solid #d9cece;
            text-align: center;
        }
        .payBtn{
            display: none;
        }
    </style>
</head>
<body>
<h1>付款成功</h1>
<div><a href="#" id="jumpBtn">查看列表</a></div>
<script>
    $(function () {
        $("#jumpBtn").on("click",function(){
            window.location.href="/index.jsp";
        });
    });
</script>
</body>
</html>