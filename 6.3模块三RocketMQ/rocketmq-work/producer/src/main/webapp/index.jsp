<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>查看订单列表</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <style type="text/css">
        table{
            border: 1px solid #d9cece;
        }
        th,td{
            width: 100px;
            border: 1px solid #d9cece;
            text-align: center;
        }
        .saveBtn{
            display: none;
        }
    </style>
</head>
<body>
<h2>我是服务器：${pageContext.request.localPort}</h2>
<h2>当前sessionId：${pageContext.session.id}</h2>
<div style="height: 95%">
    <div><a href="#" id="addBtn">新增订单</a></div>
    <form method="post" action="http://192.168.3.21:8082/order/add" enctype="application/x-www-form-urlencoded">
        <table id="resumeTable">
        </table>
    </form>
</div>
<script>
    $(function () {
        initTable();

        $(".modBtn").on("click",function(){
            $(this).parents("tr").find("input[name !='id']").removeAttr("readonly");
            $(this).next(".saveBtn").show();
            $(this).hide();
        })
        $(".saveBtn").on("click",function(){
            var allInput = $(this).parents("tr").find("input");
            saveResume(allInput);
        });
        $(".delBtn").on("click",function(){
            var idInput = $(this).parents("tr").find("input[name ='id']");
            var id =$(idInput).val();
            delResume(id);
        });
        $("#addBtn").on("click",function(){
            var aa = "<tr> "+
                "<td><input name='commodity'  /></td>"+
                "<td><input name='orderId'   /></td>"+
                "<td><input name='status'  /></td>"+
                "<td></td>"+
                "<td><input type='submit' value='保存'></td>"+
                "</tr>";
            $("#resumeTable").append(aa);
        })
    });

    function addSaveResume(_this) {
        var allInput = $(_this).parents("tr").find("input");
        var  commodity =$(allInput[0]).val();
        var  orderId =$(allInput[1]).val();
        var  status =$(allInput[2]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/order/add',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"orderId":orderId,"commodity":commodity,
                "status":status}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function delResume(id) {
        $.ajax({
            url: '${pageContext.request.contextPath}/order/del/'+id,
            type: 'get',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function saveResume(allInput) {
        var  commodity =$(allInput[0]).val();
        var  orderId =$(allInput[1]).val();
        var  status =$(allInput[2]).val();
        $.ajax({
            url: '${pageContext.request.contextPath}/order/mod',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: JSON.stringify({"orderId":orderId,"commodity":commodity,
                "status":status}),
            contentType: 'application/json;charset=utf-8',
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                initTable();
            }
        })
    }
    function initTable() {
        $.ajax({
            url: '${pageContext.request.contextPath}/order/orders',
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            data: {},
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                $("#resumeTable").empty();
                var aa="<tr><th>商品id</th><th>订单id</th><th>订单状态</th></tr>";
                for (var i=0;i<data.length;i++) {
                    aa += "<tr> "+
                        "<td>"+data[i].commodity+"</td>"+
                        "<td>"+data[i].orderId+"</td>"+
                        "<td>"+data[i].status+"</td>"+
                        "<td></td>"+
                        "</tr>";
                }
                $("#resumeTable").append(aa);
            }
        })
    }
</script>
</body>
</html>