package com.lagou.edu.mapper;

import com.lagou.edu.model.OrderInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 13:58.
 */
public interface OrderInfoMapper {
    OrderInfo selectByOrderId(@Param("orderId")Integer orderId);

    /**
     * 根据orderId修改status
     * @param orderInfo
     * @return
     */
    int updateStatus(OrderInfo orderInfo);

    void insert(OrderInfo orderInfo);

    List<OrderInfo> findAll();
}
