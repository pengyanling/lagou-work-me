package com.lagou.edu.model;

import lombok.Data;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 14:05.
 */
@Data
public class Warehouse {
    private int commodity;//商品id
    private int num;
}
