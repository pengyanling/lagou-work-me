package com.lagou.edu.mapper;

import com.lagou.edu.model.Warehouse;
import org.springframework.data.repository.query.Param;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 14:05.
 */
public interface WarehouseMapper {
    Warehouse selectByPrimaryKey(@Param("commodity")int commodity);

    int updateNum(Warehouse warehouse);
}
