package com.lagou.edu.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 13:58.
 */
@Data
@ToString
public class OrderInfo implements Serializable {
    private int commodity;//商品id
    private int status;
    private int orderId;
}
