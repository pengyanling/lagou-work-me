package com.lagou.edu.listener;

import com.lagou.edu.mapper.OrderInfoMapper;
import com.lagou.edu.mapper.WarehouseMapper;
import com.lagou.edu.model.OrderInfo;
import com.lagou.edu.model.Warehouse;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 13:57.
 */

@Component
@RocketMQMessageListener(topic = "springboot-mq-2",consumerGroup = "mq-consumer-1")
public class Consumer implements RocketMQListener<OrderInfo> {

    @Autowired
    OrderInfoMapper orderInfoMapper;

    @Autowired
    WarehouseMapper warehouseMapper;
    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public void onMessage(OrderInfo orderInfo) {
        System.out.println("消费订单消息");

        orderInfoMapper.insert(orderInfo);
        System.out.println(orderInfo.getOrderId()+"订单入库");
        Warehouse warehouse = warehouseMapper.selectByPrimaryKey(orderInfo.getCommodity());
        warehouse.setNum(warehouse.getNum()-1);
        System.out.println("扣减库存");
        warehouseMapper.updateNum(warehouse);

        redisTemplate.opsForValue().set("order:"+orderInfo.getOrderId(),"",15, TimeUnit.SECONDS);

    }

}


