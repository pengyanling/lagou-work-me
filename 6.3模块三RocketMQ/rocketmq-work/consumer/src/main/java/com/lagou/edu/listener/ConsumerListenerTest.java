package com.lagou.edu.listener;

import com.lagou.edu.model.OrderInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQListener;

/**
 * @author pengyanling
 * @createTime 2020年11月23日 13:47.
 */
@Slf4j
//@Component
//@RocketMQMessageListener(topic = "springboot-mq-2", consumerGroup = "consumer-grp-1")
public class ConsumerListenerTest implements RocketMQListener<OrderInfo> {
    @Override
    public void onMessage(OrderInfo message) {
        log.info("Receive message：" + message.toString());
    }
}
